using System.Runtime.InteropServices;
using Alma.NetWrappers;
#pragma warning disable 169 // Suppress warning message for unused variable
#pragma warning disable 649 // Field is never assigned to, and will always have its default value null

namespace Alma.NetWrappers2D
{
    public partial class CutPartFactory
    {
        public enum ImportStatus
        {
            InvalidFile = -2,
            FileNotFound = -1,
            Undefined = 0,
            InitializationOk = 1,
            ImportOk = 2
        };

        public enum ExportStatus
        {
            ExportOk = 0,
            FileError = 1,
            ExportError = 2
        };

        public enum PartStatus
        {
            PartToUpdate = 0,
            PartNotBuilt = 1,
            PartOk = 2,
            PartInvalidRead = 3,
            PartSelfIntersect = 4,
            PartNotClosed = 5
        };

        public enum Machining
        {
            UnknownType = -2,
            IgnoredType = -1,
            CutType = 0,
            MarkType = 1,
            DecorationType = 2,
            ToolTrajectoryType = 3,
            ToolOutlineType = 4,
            TextOutlineType = 5,
            FoldType = 6
        };

        public enum BevelType
        {
            UnknownBevel = -1,
            EmptyBevel = 0,
            ThickVBevel = 1,
            ThinVBevel = 2,
            ThickYBevel = 3,
            ThinYBevel = 4,
            XBevel = 5,
            KBevel = 6,
            ProgressiveBevel = 8,
            ThickJBevel = 9,
            ThinJBevel = 10,
            SpecialBevel = 11
        };

        public class PartFactoryPtr : Wrappable
        {
            internal PartFactoryPtr() { }
            ~PartFactoryPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        CutPartFactory.DeletePartFactoryInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class PartFactoriesPtr : Wrappable
        {
            internal PartFactoriesPtr() { }
            ~PartFactoriesPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        CutPartFactory.DeletePartFactoriesInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class DprImporterPtr : Wrappable
        {
            internal DprImporterPtr() { }
            ~DprImporterPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        CutPartFactory.DeleteDprImporterInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class P01ImporterPtr : Wrappable
        {
            internal P01ImporterPtr() { }
            ~P01ImporterPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        CutPartFactory.DeleteP01ImporterInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class TopoImporterPtr : Wrappable
        {
            internal TopoImporterPtr() { }
            ~TopoImporterPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        CutPartFactory.DeleteTopoImporterInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class DxfImporterPtr : Wrappable
        {
            internal DxfImporterPtr() { }
            ~DxfImporterPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        CutPartFactory.DeleteDxfImporterInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class DprExporterPtr : Wrappable
        {
            internal DprExporterPtr() { }
            ~DprExporterPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        CutPartFactory.DeleteDprExporterInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class EmfExporterPtr : Wrappable
        {
            internal EmfExporterPtr() { }
            ~EmfExporterPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        CutPartFactory.DeleteEmfExporterInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class PngExporterPtr : Wrappable
        {
            internal PngExporterPtr() { }
            ~PngExporterPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        CutPartFactory.DeletePngExporterInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class PartFactoriesIteratorPtr : Wrappable
        {
            internal PartFactoriesIteratorPtr() { }
            ~PartFactoriesIteratorPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        CutPartFactory.DeletePartFactoriesIteratorInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class IniImporterPtr : Wrappable
        {
            internal IniImporterPtr() { }
            ~IniImporterPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        CutPartFactory.DeleteIniImporterInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class DplImporterPtr : Wrappable
        {
            internal DplImporterPtr() { }
            ~DplImporterPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        CutPartFactory.DeleteDplImporterInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewDprImporter@4")]
        private static extern System.IntPtr NewDprImporterInternal(string filename);

        public static DprImporterPtr NewDprImporter(string filename)
        {
            System.IntPtr temp_object_value = NewDprImporterInternal(filename);
            DprImporterPtr temp_object = Wrappable.Create<DprImporterPtr>(temp_object_value, new DprImporterPtr());
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DprImporterImport@4")]
        private static extern void DprImporterImportInternal(System.IntPtr importer);

        public static void DprImporterImport(DprImporterPtr importer)
        {
            DprImporterImportInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetDprImporterStatus@4")]
        private static extern ImportStatus GetDprImporterStatusInternal(System.IntPtr importer);

        public static ImportStatus GetDprImporterStatus(DprImporterPtr importer)
        {
            ImportStatus returned_value = GetDprImporterStatusInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateDprMachiningToCpfMachiningByDefault@4")]
        private static extern void AssociateDprMachiningToCpfMachiningByDefaultInternal(System.IntPtr importer);

        public static void AssociateDprMachiningToCpfMachiningByDefault(DprImporterPtr importer)
        {
            AssociateDprMachiningToCpfMachiningByDefaultInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateDprMachiningToCpfMachining@12")]
        private static extern void AssociateDprMachiningToCpfMachiningInternal(System.IntPtr importer, int dpr_machining, Machining cpf_machining);

        public static void AssociateDprMachiningToCpfMachining(DprImporterPtr importer, int dpr_machining, Machining cpf_machining)
        {
            AssociateDprMachiningToCpfMachiningInternal(importer.__Ptr, dpr_machining, cpf_machining);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateDprMachiningToCpfColor@12")]
        private static extern void AssociateDprMachiningToCpfColorInternal(System.IntPtr importer, int dpr_machining, int color);

        public static void AssociateDprMachiningToCpfColor(DprImporterPtr importer, int dpr_machining, int color)
        {
            AssociateDprMachiningToCpfColorInternal(importer.__Ptr, dpr_machining, color);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateDprMachiningToCpfTool@12")]
        private static extern void AssociateDprMachiningToCpfToolInternal(System.IntPtr importer, int dpr_machining, int tool);

        public static void AssociateDprMachiningToCpfTool(DprImporterPtr importer, int dpr_machining, int tool)
        {
            AssociateDprMachiningToCpfToolInternal(importer.__Ptr, dpr_machining, tool);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateDprToolToCpfTool@12")]
        private static extern void AssociateDprToolToCpfToolInternal(System.IntPtr importer, string dpr_tool, int cpf_tool);

        public static void AssociateDprToolToCpfTool(DprImporterPtr importer, string dpr_tool, int cpf_tool)
        {
            AssociateDprToolToCpfToolInternal(importer.__Ptr, dpr_tool, cpf_tool);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateDprNullToolToCpfTool@8")]
        private static extern void AssociateDprNullToolToCpfToolInternal(System.IntPtr importer, int cpf_tool);

        public static void AssociateDprNullToolToCpfTool(DprImporterPtr importer, int cpf_tool)
        {
            AssociateDprNullToolToCpfToolInternal(importer.__Ptr, cpf_tool);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewDprPartFactory@4")]
        private static extern System.IntPtr NewDprPartFactoryInternal(System.IntPtr importer);

        public static PartFactoryPtr NewDprPartFactory(DprImporterPtr importer)
        {
            System.IntPtr temp_object_value = NewDprPartFactoryInternal(importer.__Ptr);
            PartFactoryPtr temp_object = Wrappable.Create<PartFactoryPtr>(temp_object_value, new PartFactoryPtr());
            System.GC.KeepAlive(importer);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DeleteDprImporter@4")]
        private static extern void DeleteDprImporterInternal(System.IntPtr importer);

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewDxfImporter@4")]
        private static extern System.IntPtr NewDxfImporterInternal(string filename);

        public static DxfImporterPtr NewDxfImporter(string filename)
        {
            System.IntPtr temp_object_value = NewDxfImporterInternal(filename);
            DxfImporterPtr temp_object = Wrappable.Create<DxfImporterPtr>(temp_object_value, new DxfImporterPtr());
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DxfImporterSetApproximationPrecision@12")]
        private static extern void DxfImporterSetApproximationPrecisionInternal(System.IntPtr importer, double precision);

        public static void DxfImporterSetApproximationPrecision(DxfImporterPtr importer, double precision)
        {
            DxfImporterSetApproximationPrecisionInternal(importer.__Ptr, precision);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DxfImporterInitialize@4")]
        private static extern void DxfImporterInitializeInternal(System.IntPtr importer);

        public static void DxfImporterInitialize(DxfImporterPtr importer)
        {
            DxfImporterInitializeInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetDxfLayersCount@4")]
        private static extern int GetDxfLayersCountInternal(System.IntPtr importer);

        public static int GetDxfLayersCount(DxfImporterPtr importer)
        {
            int returned_value = GetDxfLayersCountInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetDxfLayerName@16")]
        private static extern bool GetDxfLayerNameInternal(System.IntPtr importer, int layer_index, System.Text.StringBuilder out_layer_name, int layer_name_size);

        public static bool GetDxfLayerName(DxfImporterPtr importer, int layer_index, System.Text.StringBuilder out_layer_name, int layer_name_size)
        {
            bool returned_value = GetDxfLayerNameInternal(importer.__Ptr, layer_index, out_layer_name, layer_name_size);
            System.GC.KeepAlive(importer);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetDxfColorsCount@4")]
        private static extern int GetDxfColorsCountInternal(System.IntPtr importer);

        public static int GetDxfColorsCount(DxfImporterPtr importer)
        {
            int returned_value = GetDxfColorsCountInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetDxfColor@12")]
        private static extern bool GetDxfColorInternal(System.IntPtr importer, int color_index, out int out_color);

        public static bool GetDxfColor(DxfImporterPtr importer, int color_index, out int out_color)
        {
            bool returned_value = GetDxfColorInternal(importer.__Ptr, color_index, out out_color);
            System.GC.KeepAlive(importer);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetDxfLayersColorsCount@4")]
        private static extern int GetDxfLayersColorsCountInternal(System.IntPtr importer);

        public static int GetDxfLayersColorsCount(DxfImporterPtr importer)
        {
            int returned_value = GetDxfLayersColorsCountInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetDxfLayerColor@20")]
        private static extern bool GetDxfLayerColorInternal(System.IntPtr importer, int layer_color_index, System.Text.StringBuilder out_layer_name, int layer_name_size, out int out_color);

        public static bool GetDxfLayerColor(DxfImporterPtr importer, int layer_color_index, System.Text.StringBuilder out_layer_name, int layer_name_size, out int out_color)
        {
            bool returned_value = GetDxfLayerColorInternal(importer.__Ptr, layer_color_index, out_layer_name, layer_name_size, out out_color);
            System.GC.KeepAlive(importer);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DxfIsInUnknownUnit@4")]
        private static extern bool DxfIsInUnknownUnitInternal(System.IntPtr importer);

        public static bool DxfIsInUnknownUnit(DxfImporterPtr importer)
        {
            bool returned_value = DxfIsInUnknownUnitInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DxfIsInImperialUnit@4")]
        private static extern bool DxfIsInImperialUnitInternal(System.IntPtr importer);

        public static bool DxfIsInImperialUnit(DxfImporterPtr importer)
        {
            bool returned_value = DxfIsInImperialUnitInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DxfSetImperialUnit@4")]
        private static extern void DxfSetImperialUnitInternal(System.IntPtr importer);

        public static void DxfSetImperialUnit(DxfImporterPtr importer)
        {
            DxfSetImperialUnitInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DxfSetMetricUnit@4")]
        private static extern void DxfSetMetricUnitInternal(System.IntPtr importer);

        public static void DxfSetMetricUnit(DxfImporterPtr importer)
        {
            DxfSetMetricUnitInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DxfImporterImport@4")]
        private static extern void DxfImporterImportInternal(System.IntPtr importer);

        public static void DxfImporterImport(DxfImporterPtr importer)
        {
            DxfImporterImportInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetDxfImporterStatus@4")]
        private static extern ImportStatus GetDxfImporterStatusInternal(System.IntPtr importer);

        public static ImportStatus GetDxfImporterStatus(DxfImporterPtr importer)
        {
            ImportStatus returned_value = GetDxfImporterStatusInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateDxfLayerToCpfMachining@12")]
        private static extern void AssociateDxfLayerToCpfMachiningInternal(System.IntPtr importer, string layer_name, Machining cpf_machining);

        public static void AssociateDxfLayerToCpfMachining(DxfImporterPtr importer, string layer_name, Machining cpf_machining)
        {
            AssociateDxfLayerToCpfMachiningInternal(importer.__Ptr, layer_name, cpf_machining);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateDxfLayerColorToCpfMachining@16")]
        private static extern void AssociateDxfLayerColorToCpfMachiningInternal(System.IntPtr importer, string layer_name, int color, Machining machining);

        public static void AssociateDxfLayerColorToCpfMachining(DxfImporterPtr importer, string layer_name, int color, Machining machining)
        {
            AssociateDxfLayerColorToCpfMachiningInternal(importer.__Ptr, layer_name, color, machining);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateDxfColorToCpfMachining@12")]
        private static extern void AssociateDxfColorToCpfMachiningInternal(System.IntPtr importer, int color, Machining machining);

        public static void AssociateDxfColorToCpfMachining(DxfImporterPtr importer, int color, Machining machining)
        {
            AssociateDxfColorToCpfMachiningInternal(importer.__Ptr, color, machining);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateDxfLayerTextsToCpfMachining@12")]
        private static extern void AssociateDxfLayerTextsToCpfMachiningInternal(System.IntPtr importer, string layer_name, Machining cpf_machining);

        public static void AssociateDxfLayerTextsToCpfMachining(DxfImporterPtr importer, string layer_name, Machining cpf_machining)
        {
            AssociateDxfLayerTextsToCpfMachiningInternal(importer.__Ptr, layer_name, cpf_machining);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateDxfLayerToCpfColor@12")]
        private static extern void AssociateDxfLayerToCpfColorInternal(System.IntPtr importer, string layer_name, int color);

        public static void AssociateDxfLayerToCpfColor(DxfImporterPtr importer, string layer_name, int color)
        {
            AssociateDxfLayerToCpfColorInternal(importer.__Ptr, layer_name, color);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateDxfLayerToCpfTool@12")]
        private static extern void AssociateDxfLayerToCpfToolInternal(System.IntPtr importer, string layer_name, int tool);

        public static void AssociateDxfLayerToCpfTool(DxfImporterPtr importer, string layer_name, int tool)
        {
            AssociateDxfLayerToCpfToolInternal(importer.__Ptr, layer_name, tool);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewDxfPartFactory@4")]
        private static extern System.IntPtr NewDxfPartFactoryInternal(System.IntPtr importer);

        public static PartFactoryPtr NewDxfPartFactory(DxfImporterPtr importer)
        {
            System.IntPtr temp_object_value = NewDxfPartFactoryInternal(importer.__Ptr);
            PartFactoryPtr temp_object = Wrappable.Create<PartFactoryPtr>(temp_object_value, new PartFactoryPtr());
            System.GC.KeepAlive(importer);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DeleteDxfImporter@4")]
        private static extern void DeleteDxfImporterInternal(System.IntPtr importer);

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewP01Importer@4")]
        private static extern System.IntPtr NewP01ImporterInternal(string filename);

        public static P01ImporterPtr NewP01Importer(string filename)
        {
            System.IntPtr temp_object_value = NewP01ImporterInternal(filename);
            P01ImporterPtr temp_object = Wrappable.Create<P01ImporterPtr>(temp_object_value, new P01ImporterPtr());
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_P01ImporterImport@4")]
        private static extern void P01ImporterImportInternal(System.IntPtr importer);

        public static void P01ImporterImport(P01ImporterPtr importer)
        {
            P01ImporterImportInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateCpfMachiningToAllProfilesFromP01@8")]
        private static extern void AssociateCpfMachiningToAllProfilesFromP01Internal(System.IntPtr importer, Machining cpf_machining);

        public static void AssociateCpfMachiningToAllProfilesFromP01(P01ImporterPtr importer, Machining cpf_machining)
        {
            AssociateCpfMachiningToAllProfilesFromP01Internal(importer.__Ptr, cpf_machining);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewP01PartFactory@4")]
        private static extern System.IntPtr NewP01PartFactoryInternal(System.IntPtr importer);

        public static PartFactoryPtr NewP01PartFactory(P01ImporterPtr importer)
        {
            System.IntPtr temp_object_value = NewP01PartFactoryInternal(importer.__Ptr);
            PartFactoryPtr temp_object = Wrappable.Create<PartFactoryPtr>(temp_object_value, new PartFactoryPtr());
            System.GC.KeepAlive(importer);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DeleteP01Importer@4")]
        private static extern void DeleteP01ImporterInternal(System.IntPtr importer);

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewTopoImporterFromProfile@4")]
        private static extern System.IntPtr NewTopoImporterFromProfileInternal(System.IntPtr profile2);

        public static TopoImporterPtr NewTopoImporterFromProfile(Topo2d.Profile profile2)
        {
            System.IntPtr temp_object_value = NewTopoImporterFromProfileInternal(profile2.__Ptr);
            TopoImporterPtr temp_object = Wrappable.Create<TopoImporterPtr>(temp_object_value, new TopoImporterPtr());
            System.GC.KeepAlive(profile2);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewTopoImporterFromFace@4")]
        private static extern System.IntPtr NewTopoImporterFromFaceInternal(System.IntPtr face2);

        public static TopoImporterPtr NewTopoImporterFromFace(Topo2d.Face face2)
        {
            System.IntPtr temp_object_value = NewTopoImporterFromFaceInternal(face2.__Ptr);
            TopoImporterPtr temp_object = Wrappable.Create<TopoImporterPtr>(temp_object_value, new TopoImporterPtr());
            System.GC.KeepAlive(face2);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewTopoImporterFromPart@4")]
        private static extern System.IntPtr NewTopoImporterFromPartInternal(System.IntPtr part2);

        public static TopoImporterPtr NewTopoImporterFromPart(Topo2d.Part part2)
        {
            System.IntPtr temp_object_value = NewTopoImporterFromPartInternal(part2.__Ptr);
            TopoImporterPtr temp_object = Wrappable.Create<TopoImporterPtr>(temp_object_value, new TopoImporterPtr());
            System.GC.KeepAlive(part2);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_TopoImporterImport@4")]
        private static extern void TopoImporterImportInternal(System.IntPtr importer);

        public static void TopoImporterImport(TopoImporterPtr importer)
        {
            TopoImporterImportInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SetCpfMachiningToAllEntitiesFromTopo@8")]
        private static extern void SetCpfMachiningToAllEntitiesFromTopoInternal(System.IntPtr importer, Machining cpf_machining);

        public static void SetCpfMachiningToAllEntitiesFromTopo(TopoImporterPtr importer, Machining cpf_machining)
        {
            SetCpfMachiningToAllEntitiesFromTopoInternal(importer.__Ptr, cpf_machining);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateTopoMachiningToCpfMachiningByDefault@4")]
        private static extern void AssociateTopoMachiningToCpfMachiningByDefaultInternal(System.IntPtr importer);

        public static void AssociateTopoMachiningToCpfMachiningByDefault(TopoImporterPtr importer)
        {
            AssociateTopoMachiningToCpfMachiningByDefaultInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateTopoMachiningToCpfMachining@12")]
        private static extern void AssociateTopoMachiningToCpfMachiningInternal(System.IntPtr importer, int machining, Machining cpf_machining);

        public static void AssociateTopoMachiningToCpfMachining(TopoImporterPtr importer, int machining, Machining cpf_machining)
        {
            AssociateTopoMachiningToCpfMachiningInternal(importer.__Ptr, machining, cpf_machining);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewTopoPartFactory@4")]
        private static extern System.IntPtr NewTopoPartFactoryInternal(System.IntPtr importer);

        public static PartFactoryPtr NewTopoPartFactory(TopoImporterPtr importer)
        {
            System.IntPtr temp_object_value = NewTopoPartFactoryInternal(importer.__Ptr);
            PartFactoryPtr temp_object = Wrappable.Create<PartFactoryPtr>(temp_object_value, new PartFactoryPtr());
            System.GC.KeepAlive(importer);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DeleteTopoImporter@4")]
        private static extern void DeleteTopoImporterInternal(System.IntPtr importer);

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetProfiles@8")]
        private static extern System.IntPtr GetProfilesInternal(System.IntPtr part_factory_ptr, Machining machining);

        public static Topo2d.ProfilesPtr GetProfiles(PartFactoryPtr part_factory_ptr, Machining machining)
        {
            System.IntPtr temp_object_value = GetProfilesInternal(part_factory_ptr.__Ptr, machining);
            Topo2d.ProfilesPtr temp_object = Wrappable.Create<Topo2d.ProfilesPtr>(temp_object_value, new Topo2d.ProfilesPtr());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetValidProfiles@4")]
        private static extern System.IntPtr GetValidProfilesInternal(System.IntPtr part_factory_ptr);

        public static Topo2d.ProfilesPtr GetValidProfiles(PartFactoryPtr part_factory_ptr)
        {
            System.IntPtr temp_object_value = GetValidProfilesInternal(part_factory_ptr.__Ptr);
            Topo2d.ProfilesPtr temp_object = Wrappable.Create<Topo2d.ProfilesPtr>(temp_object_value, new Topo2d.ProfilesPtr());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetCutPart@8")]
        private static extern System.IntPtr GetCutPartInternal(System.IntPtr part_factory_ptr, out PartStatus status);

        public static Topo2d.Part GetCutPart(PartFactoryPtr part_factory_ptr, out PartStatus status)
        {
            System.IntPtr temp_object_value = GetCutPartInternal(part_factory_ptr.__Ptr, out status);
            Topo2d.Part temp_object = Wrappable.Create<Topo2d.Part>(temp_object_value, new Topo2d.Part());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetCutPartFacesNumber@4")]
        private static extern int GetCutPartFacesNumberInternal(System.IntPtr part_factory_ptr);

        public static int GetCutPartFacesNumber(PartFactoryPtr part_factory_ptr)
        {
            int returned_value = GetCutPartFacesNumberInternal(part_factory_ptr.__Ptr);
            System.GC.KeepAlive(part_factory_ptr);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetCutPartOpenProfiles@4")]
        private static extern System.IntPtr GetCutPartOpenProfilesInternal(System.IntPtr part_factory_ptr);

        public static Topo2d.ProfilesPtr GetCutPartOpenProfiles(PartFactoryPtr part_factory_ptr)
        {
            System.IntPtr temp_object_value = GetCutPartOpenProfilesInternal(part_factory_ptr.__Ptr);
            Topo2d.ProfilesPtr temp_object = Wrappable.Create<Topo2d.ProfilesPtr>(temp_object_value, new Topo2d.ProfilesPtr());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetTexts@4")]
        private static extern System.IntPtr GetTextsInternal(System.IntPtr part_factory_ptr);

        public static Topo2d.TextsPtr GetTexts(PartFactoryPtr part_factory_ptr)
        {
            System.IntPtr temp_object_value = GetTextsInternal(part_factory_ptr.__Ptr);
            Topo2d.TextsPtr temp_object = Wrappable.Create<Topo2d.TextsPtr>(temp_object_value, new Topo2d.TextsPtr());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetValidTexts@4")]
        private static extern System.IntPtr GetValidTextsInternal(System.IntPtr part_factory_ptr);

        public static Topo2d.TextsPtr GetValidTexts(PartFactoryPtr part_factory_ptr)
        {
            System.IntPtr temp_object_value = GetValidTextsInternal(part_factory_ptr.__Ptr);
            Topo2d.TextsPtr temp_object = Wrappable.Create<Topo2d.TextsPtr>(temp_object_value, new Topo2d.TextsPtr());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetTextsWithMachining@8")]
        private static extern System.IntPtr GetTextsWithMachiningInternal(System.IntPtr part_factory_ptr, Machining machining);

        public static Topo2d.TextsPtr GetTextsWithMachining(PartFactoryPtr part_factory_ptr, Machining machining)
        {
            System.IntPtr temp_object_value = GetTextsWithMachiningInternal(part_factory_ptr.__Ptr, machining);
            Topo2d.TextsPtr temp_object = Wrappable.Create<Topo2d.TextsPtr>(temp_object_value, new Topo2d.TextsPtr());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetTextsButWithMachining@8")]
        private static extern System.IntPtr GetTextsButWithMachiningInternal(System.IntPtr part_factory_ptr, Machining machining);

        public static Topo2d.TextsPtr GetTextsButWithMachining(PartFactoryPtr part_factory_ptr, Machining machining)
        {
            System.IntPtr temp_object_value = GetTextsButWithMachiningInternal(part_factory_ptr.__Ptr, machining);
            Topo2d.TextsPtr temp_object = Wrappable.Create<Topo2d.TextsPtr>(temp_object_value, new Topo2d.TextsPtr());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_ExportAllTopo2dProfiles@4")]
        private static extern System.IntPtr ExportAllTopo2dProfilesInternal(System.IntPtr part_factory_ptr);

        public static Topo2d.ProfilesPtr ExportAllTopo2dProfiles(PartFactoryPtr part_factory_ptr)
        {
            System.IntPtr temp_object_value = ExportAllTopo2dProfilesInternal(part_factory_ptr.__Ptr);
            Topo2d.ProfilesPtr temp_object = Wrappable.Create<Topo2d.ProfilesPtr>(temp_object_value, new Topo2d.ProfilesPtr());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_ExportTopo2dProfilesWithMachining@8")]
        private static extern System.IntPtr ExportTopo2dProfilesWithMachiningInternal(System.IntPtr part_factory_ptr, Machining machining);

        public static Topo2d.ProfilesPtr ExportTopo2dProfilesWithMachining(PartFactoryPtr part_factory_ptr, Machining machining)
        {
            System.IntPtr temp_object_value = ExportTopo2dProfilesWithMachiningInternal(part_factory_ptr.__Ptr, machining);
            Topo2d.ProfilesPtr temp_object = Wrappable.Create<Topo2d.ProfilesPtr>(temp_object_value, new Topo2d.ProfilesPtr());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_ExportTopo2dCutPart@4")]
        private static extern System.IntPtr ExportTopo2dCutPartInternal(System.IntPtr part_factory_ptr);

        public static Topo2d.PartPtr ExportTopo2dCutPart(PartFactoryPtr part_factory_ptr)
        {
            System.IntPtr temp_object_value = ExportTopo2dCutPartInternal(part_factory_ptr.__Ptr);
            Topo2d.PartPtr temp_object = Wrappable.Create<Topo2d.PartPtr>(temp_object_value, new Topo2d.PartPtr());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetCutPartErrorPoints@12")]
        private static extern bool GetCutPartErrorPointsInternal(System.IntPtr part_factory_ptr, System.Text.StringBuilder out_errors, int errors_size);

        public static bool GetCutPartErrorPoints(PartFactoryPtr part_factory_ptr, System.Text.StringBuilder out_errors, int errors_size)
        {
            bool returned_value = GetCutPartErrorPointsInternal(part_factory_ptr.__Ptr, out_errors, errors_size);
            System.GC.KeepAlive(part_factory_ptr);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_IsOnProfile@32")]
        private static extern int IsOnProfileInternal(System.IntPtr part_factory_ptr, double x, double y, double distance, bool include_text);

        public static int IsOnProfile(PartFactoryPtr part_factory_ptr, double x, double y, double distance, bool include_text)
        {
            int returned_value = IsOnProfileInternal(part_factory_ptr.__Ptr, x, y, distance, include_text);
            System.GC.KeepAlive(part_factory_ptr);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetCpfMachiningAttribute@4")]
        private static extern Machining GetCpfMachiningAttributeInternal(System.IntPtr profile);

        public static Machining GetCpfMachiningAttribute(Topo2d.Profile profile)
        {
            Machining returned_value = GetCpfMachiningAttributeInternal(profile.__Ptr);
            System.GC.KeepAlive(profile);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetCpfRenderingAttribute@4")]
        private static extern int GetCpfRenderingAttributeInternal(System.IntPtr profile);

        public static int GetCpfRenderingAttribute(Topo2d.Profile profile)
        {
            int returned_value = GetCpfRenderingAttributeInternal(profile.__Ptr);
            System.GC.KeepAlive(profile);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetCpfToolAttributeOnEdge@4")]
        private static extern int GetCpfToolAttributeOnEdgeInternal(System.IntPtr edge);

        public static int GetCpfToolAttributeOnEdge(Topo2d.Edge edge)
        {
            int returned_value = GetCpfToolAttributeOnEdgeInternal(edge.__Ptr);
            System.GC.KeepAlive(edge);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetCpfBevelAttributeOnEdge@4")]
        private static extern BevelType GetCpfBevelAttributeOnEdgeInternal(System.IntPtr edge);

        public static BevelType GetCpfBevelAttributeOnEdge(Topo2d.Edge edge)
        {
            BevelType returned_value = GetCpfBevelAttributeOnEdgeInternal(edge.__Ptr);
            System.GC.KeepAlive(edge);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AddCpfMachiningAttributeOnProfile@8")]
        private static extern void AddCpfMachiningAttributeOnProfileInternal(System.IntPtr profile, Machining cpf_machining);

        public static void AddCpfMachiningAttributeOnProfile(Topo2d.Profile profile, Machining cpf_machining)
        {
            AddCpfMachiningAttributeOnProfileInternal(profile.__Ptr, cpf_machining);
            System.GC.KeepAlive(profile);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AddCpfMachiningAttributeOnText@8")]
        private static extern void AddCpfMachiningAttributeOnTextInternal(System.IntPtr profile, Machining cpf_machining);

        public static void AddCpfMachiningAttributeOnText(Topo2d.Text profile, Machining cpf_machining)
        {
            AddCpfMachiningAttributeOnTextInternal(profile.__Ptr, cpf_machining);
            System.GC.KeepAlive(profile);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AddCpfRenderingAttribute@8")]
        private static extern void AddCpfRenderingAttributeInternal(System.IntPtr profile, int cpf_color);

        public static void AddCpfRenderingAttribute(Topo2d.Profile profile, int cpf_color)
        {
            AddCpfRenderingAttributeInternal(profile.__Ptr, cpf_color);
            System.GC.KeepAlive(profile);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AddCpfToolAttribute@8")]
        private static extern void AddCpfToolAttributeInternal(System.IntPtr profile, int cpf_tool);

        public static void AddCpfToolAttribute(Topo2d.Profile profile, int cpf_tool)
        {
            AddCpfToolAttributeInternal(profile.__Ptr, cpf_tool);
            System.GC.KeepAlive(profile);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SetMaterialName@8")]
        private static extern void SetMaterialNameInternal(System.IntPtr part_factory, string name);

        public static void SetMaterialName(PartFactoryPtr part_factory, string name)
        {
            SetMaterialNameInternal(part_factory.__Ptr, name);
            System.GC.KeepAlive(part_factory);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetMaterialName@12")]
        private static extern bool GetMaterialNameInternal(System.IntPtr part_factory, System.Text.StringBuilder out_material_name, int layer_material_size);

        public static bool GetMaterialName(PartFactoryPtr part_factory, System.Text.StringBuilder out_material_name, int layer_material_size)
        {
            bool returned_value = GetMaterialNameInternal(part_factory.__Ptr, out_material_name, layer_material_size);
            System.GC.KeepAlive(part_factory);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SetMaterialThickness@12")]
        private static extern void SetMaterialThicknessInternal(System.IntPtr part_factory, double thickness);

        public static void SetMaterialThickness(PartFactoryPtr part_factory, double thickness)
        {
            SetMaterialThicknessInternal(part_factory.__Ptr, thickness);
            System.GC.KeepAlive(part_factory);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetMaterialThickness@4")]
        private static extern double GetMaterialThicknessInternal(System.IntPtr part_factory);

        public static double GetMaterialThickness(PartFactoryPtr part_factory)
        {
            double returned_value = GetMaterialThicknessInternal(part_factory.__Ptr);
            System.GC.KeepAlive(part_factory);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SetAuthorizations@76")]
        private static extern void SetAuthorizationsInternal(System.IntPtr p_part_factory, bool auth_orig, bool auth_symx, bool auth_symy, bool auth_symxy, bool auth_rot_auto_90, bool auth_rot_1, bool has_std_offset, double offset_left, double offset_right, double offset_bottom, double offset_top, int auth_rot_min, int auth_rot_max, int auth_rot_incr);

        public static void SetAuthorizations(PartFactoryPtr p_part_factory, bool auth_orig, bool auth_symx, bool auth_symy, bool auth_symxy, bool auth_rot_auto_90, bool auth_rot_1, bool has_std_offset, double offset_left, double offset_right, double offset_bottom, double offset_top, int auth_rot_min, int auth_rot_max, int auth_rot_incr)
        {
            SetAuthorizationsInternal(p_part_factory.__Ptr, auth_orig, auth_symx, auth_symy, auth_symxy, auth_rot_auto_90, auth_rot_1, has_std_offset, offset_left, offset_right, offset_bottom, offset_top, auth_rot_min, auth_rot_max, auth_rot_incr);
            System.GC.KeepAlive(p_part_factory);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetAuthorizations@60")]
        private static extern void GetAuthorizationsInternal(System.IntPtr p_part_factory, out bool auth_orig, out bool auth_symx, out bool auth_symy, out bool auth_symxy, out bool auth_rot_auto_90, out bool auth_rot_1, out bool has_std_offset, out double offset_left, out double offset_right, out double offset_bottom, out double offset_top, out int auth_rot_min, out int auth_rot_max, out int auth_rot_incr);

        public static void GetAuthorizations(PartFactoryPtr p_part_factory, out bool auth_orig, out bool auth_symx, out bool auth_symy, out bool auth_symxy, out bool auth_rot_auto_90, out bool auth_rot_1, out bool has_std_offset, out double offset_left, out double offset_right, out double offset_bottom, out double offset_top, out int auth_rot_min, out int auth_rot_max, out int auth_rot_incr)
        {
            GetAuthorizationsInternal(p_part_factory.__Ptr, out auth_orig, out auth_symx, out auth_symy, out auth_symxy, out auth_rot_auto_90, out auth_rot_1, out has_std_offset, out offset_left, out offset_right, out offset_bottom, out offset_top, out auth_rot_min, out auth_rot_max, out auth_rot_incr);
            System.GC.KeepAlive(p_part_factory);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_HasAuthorizations@4")]
        private static extern bool HasAuthorizationsInternal(System.IntPtr p_part_factory);

        public static bool HasAuthorizations(PartFactoryPtr p_part_factory)
        {
            bool returned_value = HasAuthorizationsInternal(p_part_factory.__Ptr);
            System.GC.KeepAlive(p_part_factory);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewEmfExporter@4")]
        private static extern System.IntPtr NewEmfExporterInternal(System.IntPtr part_factory_ptr);

        public static EmfExporterPtr NewEmfExporter(PartFactoryPtr part_factory_ptr)
        {
            System.IntPtr temp_object_value = NewEmfExporterInternal(part_factory_ptr.__Ptr);
            EmfExporterPtr temp_object = Wrappable.Create<EmfExporterPtr>(temp_object_value, new EmfExporterPtr());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SetDefaultMachiningsRenderingInEmf@4")]
        private static extern void SetDefaultMachiningsRenderingInEmfInternal(System.IntPtr exporter);

        public static void SetDefaultMachiningsRenderingInEmf(EmfExporterPtr exporter)
        {
            SetDefaultMachiningsRenderingInEmfInternal(exporter.__Ptr);
            System.GC.KeepAlive(exporter);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SetTextureFillingInEmf@8")]
        private static extern void SetTextureFillingInEmfInternal(System.IntPtr exporter, string texture_filename);

        public static void SetTextureFillingInEmf(EmfExporterPtr exporter, string texture_filename)
        {
            SetTextureFillingInEmfInternal(exporter.__Ptr, texture_filename);
            System.GC.KeepAlive(exporter);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SetOpenProfilesHighlightingInEmf@8")]
        private static extern void SetOpenProfilesHighlightingInEmfInternal(System.IntPtr exporter, bool highlight);

        public static void SetOpenProfilesHighlightingInEmf(EmfExporterPtr exporter, bool highlight)
        {
            SetOpenProfilesHighlightingInEmfInternal(exporter.__Ptr, highlight);
            System.GC.KeepAlive(exporter);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_ExportProfilesInEmf@8")]
        private static extern ExportStatus ExportProfilesInEmfInternal(System.IntPtr exporter, string filename);

        public static ExportStatus ExportProfilesInEmf(EmfExporterPtr exporter, string filename)
        {
            ExportStatus returned_value = ExportProfilesInEmfInternal(exporter.__Ptr, filename);
            System.GC.KeepAlive(exporter);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_ExportCutPartInEmf@8")]
        private static extern ExportStatus ExportCutPartInEmfInternal(System.IntPtr exporter, string filename);

        public static ExportStatus ExportCutPartInEmf(EmfExporterPtr exporter, string filename)
        {
            ExportStatus returned_value = ExportCutPartInEmfInternal(exporter.__Ptr, filename);
            System.GC.KeepAlive(exporter);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_ExportCutPartErrorsInEmf@8")]
        private static extern ExportStatus ExportCutPartErrorsInEmfInternal(System.IntPtr exporter, string filename);

        public static ExportStatus ExportCutPartErrorsInEmf(EmfExporterPtr exporter, string filename)
        {
            ExportStatus returned_value = ExportCutPartErrorsInEmfInternal(exporter.__Ptr, filename);
            System.GC.KeepAlive(exporter);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DeleteEmfExporter@4")]
        private static extern void DeleteEmfExporterInternal(System.IntPtr exporter);

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewPngExporter@4")]
        private static extern System.IntPtr NewPngExporterInternal(System.IntPtr part_factory_ptr);

        public static PngExporterPtr NewPngExporter(PartFactoryPtr part_factory_ptr)
        {
            System.IntPtr temp_object_value = NewPngExporterInternal(part_factory_ptr.__Ptr);
            PngExporterPtr temp_object = Wrappable.Create<PngExporterPtr>(temp_object_value, new PngExporterPtr());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SetDefaultMachiningsRenderingInPng@4")]
        private static extern void SetDefaultMachiningsRenderingInPngInternal(System.IntPtr exporter);

        public static void SetDefaultMachiningsRenderingInPng(PngExporterPtr exporter)
        {
            SetDefaultMachiningsRenderingInPngInternal(exporter.__Ptr);
            System.GC.KeepAlive(exporter);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SetTextureFillingInPng@8")]
        private static extern void SetTextureFillingInPngInternal(System.IntPtr exporter, string texture_filename);

        public static void SetTextureFillingInPng(PngExporterPtr exporter, string texture_filename)
        {
            SetTextureFillingInPngInternal(exporter.__Ptr, texture_filename);
            System.GC.KeepAlive(exporter);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SetOpenProfilesHighlightingInPng@8")]
        private static extern void SetOpenProfilesHighlightingInPngInternal(System.IntPtr exporter, bool highlight);

        public static void SetOpenProfilesHighlightingInPng(PngExporterPtr exporter, bool highlight)
        {
            SetOpenProfilesHighlightingInPngInternal(exporter.__Ptr, highlight);
            System.GC.KeepAlive(exporter);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_ExportProfilesInPng@8")]
        private static extern ExportStatus ExportProfilesInPngInternal(System.IntPtr exporter, string filename);

        public static ExportStatus ExportProfilesInPng(PngExporterPtr exporter, string filename)
        {
            ExportStatus returned_value = ExportProfilesInPngInternal(exporter.__Ptr, filename);
            System.GC.KeepAlive(exporter);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_ExportCutPartInPng@8")]
        private static extern ExportStatus ExportCutPartInPngInternal(System.IntPtr exporter, string filename);

        public static ExportStatus ExportCutPartInPng(PngExporterPtr exporter, string filename)
        {
            ExportStatus returned_value = ExportCutPartInPngInternal(exporter.__Ptr, filename);
            System.GC.KeepAlive(exporter);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_ExportCutPartErrorsInPng@8")]
        private static extern ExportStatus ExportCutPartErrorsInPngInternal(System.IntPtr exporter, string filename);

        public static ExportStatus ExportCutPartErrorsInPng(PngExporterPtr exporter, string filename)
        {
            ExportStatus returned_value = ExportCutPartErrorsInPngInternal(exporter.__Ptr, filename);
            System.GC.KeepAlive(exporter);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DeletePngExporter@4")]
        private static extern void DeletePngExporterInternal(System.IntPtr exporter);

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewDprExporter@4")]
        private static extern System.IntPtr NewDprExporterInternal(System.IntPtr part_factory_ptr);

        public static DprExporterPtr NewDprExporter(PartFactoryPtr part_factory_ptr)
        {
            System.IntPtr temp_object_value = NewDprExporterInternal(part_factory_ptr.__Ptr);
            DprExporterPtr temp_object = Wrappable.Create<DprExporterPtr>(temp_object_value, new DprExporterPtr());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateCpfMachiningToDprMachiningByDefault@4")]
        private static extern void AssociateCpfMachiningToDprMachiningByDefaultInternal(System.IntPtr exporter);

        public static void AssociateCpfMachiningToDprMachiningByDefault(DprExporterPtr exporter)
        {
            AssociateCpfMachiningToDprMachiningByDefaultInternal(exporter.__Ptr);
            System.GC.KeepAlive(exporter);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateCpfMachiningToDprMachining@12")]
        private static extern void AssociateCpfMachiningToDprMachiningInternal(System.IntPtr exporter, Machining machining_type, int dpr_machining);

        public static void AssociateCpfMachiningToDprMachining(DprExporterPtr exporter, Machining machining_type, int dpr_machining)
        {
            AssociateCpfMachiningToDprMachiningInternal(exporter.__Ptr, machining_type, dpr_machining);
            System.GC.KeepAlive(exporter);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateCpfToolToDprTool@12")]
        private static extern void AssociateCpfToolToDprToolInternal(System.IntPtr exporter, int cpf_tool, string dpr_tool);

        public static void AssociateCpfToolToDprTool(DprExporterPtr exporter, int cpf_tool, string dpr_tool)
        {
            AssociateCpfToolToDprToolInternal(exporter.__Ptr, cpf_tool, dpr_tool);
            System.GC.KeepAlive(exporter);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SetDprId@8")]
        private static extern void SetDprIdInternal(System.IntPtr exporter, string dpr_tool);

        public static void SetDprId(DprExporterPtr exporter, string dpr_tool)
        {
            SetDprIdInternal(exporter.__Ptr, dpr_tool);
            System.GC.KeepAlive(exporter);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_ExportProfilesInDpr@8")]
        private static extern ExportStatus ExportProfilesInDprInternal(System.IntPtr exporter, string filename);

        public static ExportStatus ExportProfilesInDpr(DprExporterPtr exporter, string filename)
        {
            ExportStatus returned_value = ExportProfilesInDprInternal(exporter.__Ptr, filename);
            System.GC.KeepAlive(exporter);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_ExportCutPartInDpr@8")]
        private static extern ExportStatus ExportCutPartInDprInternal(System.IntPtr exporter, string filename);

        public static ExportStatus ExportCutPartInDpr(DprExporterPtr exporter, string filename)
        {
            ExportStatus returned_value = ExportCutPartInDprInternal(exporter.__Ptr, filename);
            System.GC.KeepAlive(exporter);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DeleteDprExporter@4")]
        private static extern void DeleteDprExporterInternal(System.IntPtr exporter);

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SplitFaces@4")]
        private static extern System.IntPtr SplitFacesInternal(System.IntPtr part_factory_ptr);

        public static PartFactoriesPtr SplitFaces(PartFactoryPtr part_factory_ptr)
        {
            System.IntPtr temp_object_value = SplitFacesInternal(part_factory_ptr.__Ptr);
            PartFactoriesPtr temp_object = Wrappable.Create<PartFactoriesPtr>(temp_object_value, new PartFactoriesPtr());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_FactoryWithoutSheetProfile@4")]
        private static extern System.IntPtr FactoryWithoutSheetProfileInternal(System.IntPtr part_factory_ptr);

        public static PartFactoryPtr FactoryWithoutSheetProfile(PartFactoryPtr part_factory_ptr)
        {
            System.IntPtr temp_object_value = FactoryWithoutSheetProfileInternal(part_factory_ptr.__Ptr);
            PartFactoryPtr temp_object = Wrappable.Create<PartFactoryPtr>(temp_object_value, new PartFactoryPtr());
            System.GC.KeepAlive(part_factory_ptr);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_IdentifyNestingProperties@16")]
        private static extern bool IdentifyNestingPropertiesInternal(System.IntPtr part_factory_ptr, out double width, out double height, out double efficiency);

        public static bool IdentifyNestingProperties(PartFactoryPtr part_factory_ptr, out double width, out double height, out double efficiency)
        {
            bool returned_value = IdentifyNestingPropertiesInternal(part_factory_ptr.__Ptr, out width, out height, out efficiency);
            System.GC.KeepAlive(part_factory_ptr);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SavePartFactory@8")]
        private static extern void SavePartFactoryInternal(System.IntPtr part_factory_ptr, string filename);

        public static void SavePartFactory(PartFactoryPtr part_factory_ptr, string filename)
        {
            SavePartFactoryInternal(part_factory_ptr.__Ptr, filename);
            System.GC.KeepAlive(part_factory_ptr);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_LoadPartFactory@4")]
        private static extern System.IntPtr LoadPartFactoryInternal(string filename);

        public static PartFactoryPtr LoadPartFactory(string filename)
        {
            System.IntPtr temp_object_value = LoadPartFactoryInternal(filename);
            PartFactoryPtr temp_object = Wrappable.Create<PartFactoryPtr>(temp_object_value, new PartFactoryPtr());
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DeletePartFactory@4")]
        private static extern void DeletePartFactoryInternal(System.IntPtr part_factory_ptr);

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DeletePartFactories@4")]
        private static extern void DeletePartFactoriesInternal(System.IntPtr part_factories_ptr);

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewPartFactoriesIterator@4")]
        private static extern System.IntPtr NewPartFactoriesIteratorInternal(System.IntPtr x0);

        public static PartFactoriesIteratorPtr NewPartFactoriesIterator(PartFactoriesPtr x0)
        {
            System.IntPtr temp_object_value = NewPartFactoriesIteratorInternal(x0.__Ptr);
            PartFactoriesIteratorPtr temp_object = Wrappable.Create<PartFactoriesIteratorPtr>(temp_object_value, new PartFactoriesIteratorPtr());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DeletePartFactoriesIterator@4")]
        private static extern void DeletePartFactoriesIteratorInternal(System.IntPtr x0);

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_PartFactoriesIteratorHasNext@4")]
        private static extern bool PartFactoriesIteratorHasNextInternal(System.IntPtr x0);

        public static bool PartFactoriesIteratorHasNext(PartFactoriesIteratorPtr x0)
        {
            bool returned_value = PartFactoriesIteratorHasNextInternal(x0.__Ptr);
            System.GC.KeepAlive(x0);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_PartFactoriesIteratorNext@4")]
        private static extern System.IntPtr PartFactoriesIteratorNextInternal(System.IntPtr x0);

        public static PartFactoryPtr PartFactoriesIteratorNext(PartFactoriesIteratorPtr x0)
        {
            System.IntPtr temp_object_value = PartFactoriesIteratorNextInternal(x0.__Ptr);
            PartFactoryPtr temp_object = Wrappable.Create<PartFactoryPtr>(temp_object_value, new PartFactoryPtr());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewIniImporter@4")]
        private static extern System.IntPtr NewIniImporterInternal(string filename);

        public static IniImporterPtr NewIniImporter(string filename)
        {
            System.IntPtr temp_object_value = NewIniImporterInternal(filename);
            IniImporterPtr temp_object = Wrappable.Create<IniImporterPtr>(temp_object_value, new IniImporterPtr());
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SetIniDefaultConfiguration@4")]
        private static extern void SetIniDefaultConfigurationInternal(System.IntPtr importer);

        public static void SetIniDefaultConfiguration(IniImporterPtr importer)
        {
            SetIniDefaultConfigurationInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SetIniConfigValue@16")]
        private static extern void SetIniConfigValueInternal(System.IntPtr importer, string section, string key, string value);

        public static void SetIniConfigValue(IniImporterPtr importer, string section, string key, string value)
        {
            SetIniConfigValueInternal(importer.__Ptr, section, key, value);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_IniImporterImport@4")]
        private static extern void IniImporterImportInternal(System.IntPtr importer);

        public static void IniImporterImport(IniImporterPtr importer)
        {
            IniImporterImportInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetIniImporterStatus@4")]
        private static extern ImportStatus GetIniImporterStatusInternal(System.IntPtr importer);

        public static ImportStatus GetIniImporterStatus(IniImporterPtr importer)
        {
            ImportStatus returned_value = GetIniImporterStatusInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateIniMachiningToCpfMachiningByDefault@4")]
        private static extern void AssociateIniMachiningToCpfMachiningByDefaultInternal(System.IntPtr importer);

        public static void AssociateIniMachiningToCpfMachiningByDefault(IniImporterPtr importer)
        {
            AssociateIniMachiningToCpfMachiningByDefaultInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateIniMachiningToCpfMachining@12")]
        private static extern void AssociateIniMachiningToCpfMachiningInternal(System.IntPtr importer, int Ini_machining, Machining cpf_machining);

        public static void AssociateIniMachiningToCpfMachining(IniImporterPtr importer, int Ini_machining, Machining cpf_machining)
        {
            AssociateIniMachiningToCpfMachiningInternal(importer.__Ptr, Ini_machining, cpf_machining);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateIniMachiningToCpfColor@12")]
        private static extern void AssociateIniMachiningToCpfColorInternal(System.IntPtr importer, int Ini_machining, int color);

        public static void AssociateIniMachiningToCpfColor(IniImporterPtr importer, int Ini_machining, int color)
        {
            AssociateIniMachiningToCpfColorInternal(importer.__Ptr, Ini_machining, color);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateIniMachiningToCpfTool@12")]
        private static extern void AssociateIniMachiningToCpfToolInternal(System.IntPtr importer, int Ini_machining, int tool);

        public static void AssociateIniMachiningToCpfTool(IniImporterPtr importer, int Ini_machining, int tool)
        {
            AssociateIniMachiningToCpfToolInternal(importer.__Ptr, Ini_machining, tool);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateIniToolToCpfTool@28")]
        private static extern void AssociateIniToolToCpfToolInternal(System.IntPtr importer, int ini_tool, double ini_tool_value_1, double ini_tool_value_2, int cpf_tool);

        public static void AssociateIniToolToCpfTool(IniImporterPtr importer, int ini_tool, double ini_tool_value_1, double ini_tool_value_2, int cpf_tool)
        {
            AssociateIniToolToCpfToolInternal(importer.__Ptr, ini_tool, ini_tool_value_1, ini_tool_value_2, cpf_tool);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateIniFontToCpfFontByDefault@4")]
        private static extern void AssociateIniFontToCpfFontByDefaultInternal(System.IntPtr importer);

        public static void AssociateIniFontToCpfFontByDefault(IniImporterPtr importer)
        {
            AssociateIniFontToCpfFontByDefaultInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateIniFontToCpfFont@20")]
        private static extern void AssociateIniFontToCpfFontInternal(System.IntPtr importer, int ini_font_number, string font_name, int font_size, Topo2d.AttachmentType attachment_type);

        public static void AssociateIniFontToCpfFont(IniImporterPtr importer, int ini_font_number, string font_name, int font_size, Topo2d.AttachmentType attachment_type)
        {
            AssociateIniFontToCpfFontInternal(importer.__Ptr, ini_font_number, font_name, font_size, attachment_type);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewIniPartFactories@4")]
        private static extern System.IntPtr NewIniPartFactoriesInternal(System.IntPtr importer);

        public static PartFactoriesPtr NewIniPartFactories(IniImporterPtr importer)
        {
            System.IntPtr temp_object_value = NewIniPartFactoriesInternal(importer.__Ptr);
            PartFactoriesPtr temp_object = Wrappable.Create<PartFactoriesPtr>(temp_object_value, new PartFactoriesPtr());
            System.GC.KeepAlive(importer);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DeleteIniImporter@4")]
        private static extern void DeleteIniImporterInternal(System.IntPtr importer);

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewDplImporter@4")]
        private static extern System.IntPtr NewDplImporterInternal(string filename);

        public static DplImporterPtr NewDplImporter(string filename)
        {
            System.IntPtr temp_object_value = NewDplImporterInternal(filename);
            DplImporterPtr temp_object = Wrappable.Create<DplImporterPtr>(temp_object_value, new DplImporterPtr());
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_SetDplDefaultConfiguration@4")]
        private static extern void SetDplDefaultConfigurationInternal(System.IntPtr importer);

        public static void SetDplDefaultConfiguration(DplImporterPtr importer)
        {
            SetDplDefaultConfigurationInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DplImporterImport@4")]
        private static extern void DplImporterImportInternal(System.IntPtr importer);

        public static void DplImporterImport(DplImporterPtr importer)
        {
            DplImporterImportInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_GetDplImporterStatus@4")]
        private static extern ImportStatus GetDplImporterStatusInternal(System.IntPtr importer);

        public static ImportStatus GetDplImporterStatus(DplImporterPtr importer)
        {
            ImportStatus returned_value = GetDplImporterStatusInternal(importer.__Ptr);
            System.GC.KeepAlive(importer);
            return returned_value;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateDplToolToCpfTool@24")]
        private static extern void AssociateDplToolToCpfToolInternal(System.IntPtr importer, double dpl_tool_value_1, double dpl_tool_value_2, int cpf_tool);

        public static void AssociateDplToolToCpfTool(DplImporterPtr importer, double dpl_tool_value_1, double dpl_tool_value_2, int cpf_tool)
        {
            AssociateDplToolToCpfToolInternal(importer.__Ptr, dpl_tool_value_1, dpl_tool_value_2, cpf_tool);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_AssociateDplFoldLineMachiningToCpfMachining@8")]
        private static extern void AssociateDplFoldLineMachiningToCpfMachiningInternal(System.IntPtr importer, Machining cpf_machining);

        public static void AssociateDplFoldLineMachiningToCpfMachining(DplImporterPtr importer, Machining cpf_machining)
        {
            AssociateDplFoldLineMachiningToCpfMachiningInternal(importer.__Ptr, cpf_machining);
            System.GC.KeepAlive(importer);
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_NewDplPartFactory@4")]
        private static extern System.IntPtr NewDplPartFactoryInternal(System.IntPtr importer);

        public static PartFactoryPtr NewDplPartFactory(DplImporterPtr importer)
        {
            System.IntPtr temp_object_value = NewDplPartFactoryInternal(importer.__Ptr);
            PartFactoryPtr temp_object = Wrappable.Create<PartFactoryPtr>(temp_object_value, new PartFactoryPtr());
            System.GC.KeepAlive(importer);
            return temp_object;
        }

        [DllImport("cCutPartFactoryFull.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_CPF_DeleteDplImporter@4")]
        private static extern void DeleteDplImporterInternal(System.IntPtr importer);

    }
}

#pragma warning restore 169
#pragma warning restore 649
