using System.Runtime.InteropServices;
using Alma.NetWrappers;
#pragma warning disable 169 // Suppress warning message for unused variable
#pragma warning disable 649 // Field is never assigned to, and will always have its default value null

namespace Alma.NetWrappers2D
{
    public partial class Topo2d
    {
        public enum EdgeType {
                EdgeTypeSegment = 0,
                EdgeTypeArc = 1
        };

        public enum AttachmentType {
                TopLeft = 0,
                TopCenter = 1,
                TopRight = 2,
                MiddleLeft = 4,
                MiddleCenter = 5,
                MiddleRight = 6,
                BottomLeft = 8,
                BottomCenter = 9,
                BottomRight = 10
        };

        // Topo2dPoint is not a simple type.
        // Topo2dVector is not a simple type.
        public class Vertex : Wrappable
        {
            internal Vertex() { }
            ~Vertex()
            {}
            public Vertex(System.IntPtr ptr) { __Ptr = ptr; }
        }
        public class Edge : Wrappable
        {
            internal Edge() { }
            ~Edge()
            {}
            public Edge(System.IntPtr ptr) { __Ptr = ptr; }
        }
        public class Profile : Wrappable
        {
            internal Profile() { }
            ~Profile()
            {}
            public Profile(System.IntPtr ptr) { __Ptr = ptr; }
        }
        public class Profiles : Wrappable
        {
            internal Profiles() { }
            ~Profiles()
            {}
            public Profiles(System.IntPtr ptr) { __Ptr = ptr; }
        }
        public class Face : Wrappable
        {
            internal Face() { }
            ~Face()
            {}
            public Face(System.IntPtr ptr) { __Ptr = ptr; }
        }
        public class Faces : Wrappable
        {
            internal Faces() { }
            ~Faces()
            {}
            public Faces(System.IntPtr ptr) { __Ptr = ptr; }
        }
        public class Part : Wrappable
        {
            internal Part() { }
            ~Part()
            {}
            public Part(System.IntPtr ptr) { __Ptr = ptr; }
        }
        public class Parts : Wrappable
        {
            internal Parts() { }
            ~Parts()
            {}
            public Parts(System.IntPtr ptr) { __Ptr = ptr; }
        }
        public class Text : Wrappable
        {
            internal Text() { }
            ~Text()
            {}
            public Text(System.IntPtr ptr) { __Ptr = ptr; }
        }
        public class Texts : Wrappable
        {
            internal Texts() { }
            ~Texts()
            {}
            public Texts(System.IntPtr ptr) { __Ptr = ptr; }
        }
        public class ProfilePtr : Wrappable
        {
            internal ProfilePtr() { }
            ~ProfilePtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteProfileInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class FacePtr : Wrappable
        {
            internal FacePtr() { }
            ~FacePtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteFaceInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class PartPtr : Wrappable
        {
            internal PartPtr() { }
            ~PartPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeletePartInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class DprAttributePtr : Wrappable
        {
            internal DprAttributePtr() { }
            ~DprAttributePtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteDprAttributePtrInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class ProfilesPtr : Wrappable
        {
            internal ProfilesPtr() { }
            ~ProfilesPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteProfilesInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class FacesPtr : Wrappable
        {
            internal FacesPtr() { }
            ~FacesPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteFacesInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class PartsPtr : Wrappable
        {
            internal PartsPtr() { }
            ~PartsPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeletePartsInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class TextPtr : Wrappable
        {
            internal TextPtr() { }
            ~TextPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteTextInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class TextsPtr : Wrappable
        {
            internal TextsPtr() { }
            ~TextsPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteTextsInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class EdgeIteratorPtr : Wrappable
        {
            internal EdgeIteratorPtr() { }
            ~EdgeIteratorPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteEdgeIteratorInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class ProfileIteratorPtr : Wrappable
        {
            internal ProfileIteratorPtr() { }
            ~ProfileIteratorPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteProfileIteratorInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class FaceIteratorPtr : Wrappable
        {
            internal FaceIteratorPtr() { }
            ~FaceIteratorPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteFaceIteratorInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class PartIteratorPtr : Wrappable
        {
            internal PartIteratorPtr() { }
            ~PartIteratorPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeletePartIteratorInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class TextIteratorPtr : Wrappable
        {
            internal TextIteratorPtr() { }
            ~TextIteratorPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DeleteTextIteratorInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }
        public class DprWriterPtr : Wrappable
        {
            internal DprWriterPtr() { }
            ~DprWriterPtr()
            {
                if (__Ptr != System.IntPtr.Zero)
                {
                    try
                    {
                        Topo2d.DprWriterDeleteInternal(__Ptr);
                        __Ptr = System.IntPtr.Zero;
                    }
                    catch { }
                }
            }
            public System.IntPtr Ptr { get { return __Ptr; } }
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewPart@0")]
        private static extern System.IntPtr NewPartInternal();

        public static PartPtr NewPart()
        {
            System.IntPtr temp_object_value = NewPartInternal();
            PartPtr temp_object = Wrappable.Create<PartPtr>(temp_object_value, new PartPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyPart@4")]
        private static extern System.IntPtr DeepCopyPartInternal(System.IntPtr part);

        public static PartPtr DeepCopyPart(Part part)
        {
            System.IntPtr temp_object_value = DeepCopyPartInternal(part.__Ptr);
            PartPtr temp_object = Wrappable.Create<PartPtr>(temp_object_value, new PartPtr());
            System.GC.KeepAlive(part);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyPartNoAttribute@4")]
        private static extern System.IntPtr DeepCopyPartNoAttributeInternal(System.IntPtr part);

        public static PartPtr DeepCopyPartNoAttribute(Part part)
        {
            System.IntPtr temp_object_value = DeepCopyPartNoAttributeInternal(part.__Ptr);
            PartPtr temp_object = Wrappable.Create<PartPtr>(temp_object_value, new PartPtr());
            System.GC.KeepAlive(part);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dPartNewSharedOwner@4")]
        private static extern System.IntPtr PartNewSharedOwnerInternal(System.IntPtr part);

        public static PartPtr PartNewSharedOwner(PartPtr part)
        {
            System.IntPtr temp_object_value = PartNewSharedOwnerInternal(part.__Ptr);
            PartPtr temp_object = Wrappable.Create<PartPtr>(temp_object_value, new PartPtr());
            System.GC.KeepAlive(part);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddFace@8")]
        private static extern void AddFaceInternal(System.IntPtr part, System.IntPtr face);

        public static void AddFace(PartPtr part, FacePtr face)
        {
            AddFaceInternal(part.__Ptr, face.__Ptr);
            System.GC.KeepAlive(part);
            System.GC.KeepAlive(face);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeletePart@4")]
        private static extern void DeletePartInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewFace@4")]
        private static extern System.IntPtr NewFaceInternal(System.IntPtr externalProfile);

        public static FacePtr NewFace(ProfilePtr externalProfile)
        {
            System.IntPtr temp_object_value = NewFaceInternal(externalProfile.__Ptr);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            System.GC.KeepAlive(externalProfile);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyFace@4")]
        private static extern System.IntPtr DeepCopyFaceInternal(System.IntPtr face);

        public static FacePtr DeepCopyFace(Face face)
        {
            System.IntPtr temp_object_value = DeepCopyFaceInternal(face.__Ptr);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            System.GC.KeepAlive(face);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFaceNewSharedOwner@4")]
        private static extern System.IntPtr FaceNewSharedOwnerInternal(System.IntPtr face);

        public static FacePtr FaceNewSharedOwner(FacePtr face)
        {
            System.IntPtr temp_object_value = FaceNewSharedOwnerInternal(face.__Ptr);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            System.GC.KeepAlive(face);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddInternalProfile@8")]
        private static extern void AddInternalProfileInternal(System.IntPtr face, System.IntPtr profile);

        public static void AddInternalProfile(FacePtr face, ProfilePtr profile)
        {
            AddInternalProfileInternal(face.__Ptr, profile.__Ptr);
            System.GC.KeepAlive(face);
            System.GC.KeepAlive(profile);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFaceRemoveTexts@4")]
        private static extern void FaceRemoveTextsInternal(System.IntPtr face);

        public static void FaceRemoveTexts(FacePtr face)
        {
            FaceRemoveTextsInternal(face.__Ptr);
            System.GC.KeepAlive(face);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteFace@4")]
        private static extern void DeleteFaceInternal(System.IntPtr x0);

        // Topo2dNewProfile is not a simple function.

        // Topo2dNewProfileEpsilon is not a simple function.

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyProfile@4")]
        private static extern System.IntPtr DeepCopyProfileInternal(System.IntPtr profile);

        public static ProfilePtr DeepCopyProfile(Profile profile)
        {
            System.IntPtr temp_object_value = DeepCopyProfileInternal(profile.__Ptr);
            ProfilePtr temp_object = Wrappable.Create<ProfilePtr>(temp_object_value, new ProfilePtr());
            System.GC.KeepAlive(profile);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileNewSharedOwner@4")]
        private static extern System.IntPtr ProfileNewSharedOwnerInternal(System.IntPtr profile);

        public static ProfilePtr ProfileNewSharedOwner(ProfilePtr profile)
        {
            System.IntPtr temp_object_value = ProfileNewSharedOwnerInternal(profile.__Ptr);
            ProfilePtr temp_object = Wrappable.Create<ProfilePtr>(temp_object_value, new ProfilePtr());
            System.GC.KeepAlive(profile);
            return temp_object;
        }

        // Topo2dAddSegment is not a simple function.

        // Topo2dAddArc is not a simple function.

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dCloseProfile@4")]
        private static extern bool CloseProfileInternal(System.IntPtr profil);

        public static bool CloseProfile(ProfilePtr profil)
        {
            bool returned_value = CloseProfileInternal(profil.__Ptr);
            System.GC.KeepAlive(profil);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteProfile@4")]
        private static extern void DeleteProfileInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileGetEpsilon@4")]
        private static extern double ProfileGetEpsilonInternal(System.IntPtr profile);

        public static double ProfileGetEpsilon(ProfilePtr profile)
        {
            double returned_value = ProfileGetEpsilonInternal(profile.__Ptr);
            System.GC.KeepAlive(profile);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileSetEpsilon@12")]
        private static extern void ProfileSetEpsilonInternal(System.IntPtr profile, double epsilon);

        public static void ProfileSetEpsilon(ProfilePtr profile, double epsilon)
        {
            ProfileSetEpsilonInternal(profile.__Ptr, epsilon);
            System.GC.KeepAlive(profile);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyText@4")]
        private static extern System.IntPtr DeepCopyTextInternal(System.IntPtr text);

        public static TextPtr DeepCopyText(Text text)
        {
            System.IntPtr temp_object_value = DeepCopyTextInternal(text.__Ptr);
            TextPtr temp_object = Wrappable.Create<TextPtr>(temp_object_value, new TextPtr());
            System.GC.KeepAlive(text);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeepCopyTexts@4")]
        private static extern System.IntPtr DeepCopyTextsInternal(System.IntPtr texts);

        public static TextsPtr DeepCopyTexts(Texts texts)
        {
            System.IntPtr temp_object_value = DeepCopyTextsInternal(texts.__Ptr);
            TextsPtr temp_object = Wrappable.Create<TextsPtr>(temp_object_value, new TextsPtr());
            System.GC.KeepAlive(texts);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteDprAttributePtr@4")]
        private static extern void DeleteDprAttributePtrInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFaces@4")]
        private static extern System.IntPtr GetFacesInternal(System.IntPtr x0);

        public static Faces GetFaces(PartPtr x0)
        {
            System.IntPtr temp_object_value = GetFacesInternal(x0.__Ptr);
            Faces temp_object = Wrappable.Create<Faces>(temp_object_value, new Faces());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFacesOnWeak@4")]
        private static extern System.IntPtr GetFacesOnWeakInternal(System.IntPtr x0);

        public static Faces GetFacesOnWeak(Part x0)
        {
            System.IntPtr temp_object_value = GetFacesOnWeakInternal(x0.__Ptr);
            Faces temp_object = Wrappable.Create<Faces>(temp_object_value, new Faces());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetExternalProfile@4")]
        private static extern System.IntPtr GetExternalProfileInternal(System.IntPtr x0);

        public static Profile GetExternalProfile(FacePtr x0)
        {
            System.IntPtr temp_object_value = GetExternalProfileInternal(x0.__Ptr);
            Profile temp_object = Wrappable.Create<Profile>(temp_object_value, new Profile());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetExternalProfileOnWeak@4")]
        private static extern System.IntPtr GetExternalProfileOnWeakInternal(System.IntPtr x0);

        public static Profile GetExternalProfileOnWeak(Face x0)
        {
            System.IntPtr temp_object_value = GetExternalProfileOnWeakInternal(x0.__Ptr);
            Profile temp_object = Wrappable.Create<Profile>(temp_object_value, new Profile());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetInternalProfiles@4")]
        private static extern System.IntPtr GetInternalProfilesInternal(System.IntPtr x0);

        public static Profiles GetInternalProfiles(FacePtr x0)
        {
            System.IntPtr temp_object_value = GetInternalProfilesInternal(x0.__Ptr);
            Profiles temp_object = Wrappable.Create<Profiles>(temp_object_value, new Profiles());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetInternalProfilesOnWeak@4")]
        private static extern System.IntPtr GetInternalProfilesOnWeakInternal(System.IntPtr x0);

        public static Profiles GetInternalProfilesOnWeak(Face x0)
        {
            System.IntPtr temp_object_value = GetInternalProfilesOnWeakInternal(x0.__Ptr);
            Profiles temp_object = Wrappable.Create<Profiles>(temp_object_value, new Profiles());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetTexts@4")]
        private static extern System.IntPtr GetTextsInternal(System.IntPtr x0);

        public static Texts GetTexts(FacePtr x0)
        {
            System.IntPtr temp_object_value = GetTextsInternal(x0.__Ptr);
            Texts temp_object = Wrappable.Create<Texts>(temp_object_value, new Texts());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetTextsOnWeak@4")]
        private static extern System.IntPtr GetTextsOnWeakInternal(System.IntPtr x0);

        public static Texts GetTextsOnWeak(Face x0)
        {
            System.IntPtr temp_object_value = GetTextsOnWeakInternal(x0.__Ptr);
            Texts temp_object = Wrappable.Create<Texts>(temp_object_value, new Texts());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewEdgeIterator@4")]
        private static extern System.IntPtr NewEdgeIteratorInternal(System.IntPtr x0);

        public static EdgeIteratorPtr NewEdgeIterator(ProfilePtr x0)
        {
            System.IntPtr temp_object_value = NewEdgeIteratorInternal(x0.__Ptr);
            EdgeIteratorPtr temp_object = Wrappable.Create<EdgeIteratorPtr>(temp_object_value, new EdgeIteratorPtr());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewEdgeIteratorOnWeak@4")]
        private static extern System.IntPtr NewEdgeIteratorOnWeakInternal(System.IntPtr x0);

        public static EdgeIteratorPtr NewEdgeIteratorOnWeak(Profile x0)
        {
            System.IntPtr temp_object_value = NewEdgeIteratorOnWeakInternal(x0.__Ptr);
            EdgeIteratorPtr temp_object = Wrappable.Create<EdgeIteratorPtr>(temp_object_value, new EdgeIteratorPtr());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteEdgeIterator@4")]
        private static extern void DeleteEdgeIteratorInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dEdgeIteratorHasNext@4")]
        private static extern bool EdgeIteratorHasNextInternal(System.IntPtr x0);

        public static bool EdgeIteratorHasNext(EdgeIteratorPtr x0)
        {
            bool returned_value = EdgeIteratorHasNextInternal(x0.__Ptr);
            System.GC.KeepAlive(x0);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dEdgeIteratorNext@4")]
        private static extern System.IntPtr EdgeIteratorNextInternal(System.IntPtr x0);

        public static Edge EdgeIteratorNext(EdgeIteratorPtr x0)
        {
            System.IntPtr temp_object_value = EdgeIteratorNextInternal(x0.__Ptr);
            Edge temp_object = Wrappable.Create<Edge>(temp_object_value, new Edge());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetEdgeType@4")]
        private static extern EdgeType GetEdgeTypeInternal(System.IntPtr x0);

        public static EdgeType GetEdgeType(Edge x0)
        {
            EdgeType returned_value = GetEdgeTypeInternal(x0.__Ptr);
            System.GC.KeepAlive(x0);
            return returned_value;
        }

        // Topo2dGetSegment is not a simple function.

        // Topo2dGetArc is not a simple function.

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetPreviousVertex@4")]
        private static extern System.IntPtr GetPreviousVertexInternal(System.IntPtr edge);

        public static Vertex GetPreviousVertex(Edge edge)
        {
            System.IntPtr temp_object_value = GetPreviousVertexInternal(edge.__Ptr);
            Vertex temp_object = Wrappable.Create<Vertex>(temp_object_value, new Vertex());
            System.GC.KeepAlive(edge);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetNextVertex@4")]
        private static extern System.IntPtr GetNextVertexInternal(System.IntPtr edge);

        public static Vertex GetNextVertex(Edge edge)
        {
            System.IntPtr temp_object_value = GetNextVertexInternal(edge.__Ptr);
            Vertex temp_object = Wrappable.Create<Vertex>(temp_object_value, new Vertex());
            System.GC.KeepAlive(edge);
            return temp_object;
        }

        // Topo2dGetPosition is not a simple function.

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertProfile@4")]
        private static extern System.IntPtr ConvertProfileInternal(System.IntPtr x0);

        public static Profile ConvertProfile(ProfilePtr x0)
        {
            System.IntPtr temp_object_value = ConvertProfileInternal(x0.__Ptr);
            Profile temp_object = Wrappable.Create<Profile>(temp_object_value, new Profile());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertProfiles@4")]
        private static extern System.IntPtr ConvertProfilesInternal(System.IntPtr x0);

        public static Profiles ConvertProfiles(ProfilesPtr x0)
        {
            System.IntPtr temp_object_value = ConvertProfilesInternal(x0.__Ptr);
            Profiles temp_object = Wrappable.Create<Profiles>(temp_object_value, new Profiles());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertFace@4")]
        private static extern System.IntPtr ConvertFaceInternal(System.IntPtr x0);

        public static Face ConvertFace(FacePtr x0)
        {
            System.IntPtr temp_object_value = ConvertFaceInternal(x0.__Ptr);
            Face temp_object = Wrappable.Create<Face>(temp_object_value, new Face());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertFaces@4")]
        private static extern System.IntPtr ConvertFacesInternal(System.IntPtr faces);

        public static Faces ConvertFaces(FacesPtr faces)
        {
            System.IntPtr temp_object_value = ConvertFacesInternal(faces.__Ptr);
            Faces temp_object = Wrappable.Create<Faces>(temp_object_value, new Faces());
            System.GC.KeepAlive(faces);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertPart@4")]
        private static extern System.IntPtr ConvertPartInternal(System.IntPtr x0);

        public static Part ConvertPart(PartPtr x0)
        {
            System.IntPtr temp_object_value = ConvertPartInternal(x0.__Ptr);
            Part temp_object = Wrappable.Create<Part>(temp_object_value, new Part());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertParts@4")]
        private static extern System.IntPtr ConvertPartsInternal(System.IntPtr parts);

        public static Parts ConvertParts(PartsPtr parts)
        {
            System.IntPtr temp_object_value = ConvertPartsInternal(parts.__Ptr);
            Parts temp_object = Wrappable.Create<Parts>(temp_object_value, new Parts());
            System.GC.KeepAlive(parts);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertText@4")]
        private static extern System.IntPtr ConvertTextInternal(System.IntPtr x0);

        public static Text ConvertText(TextPtr x0)
        {
            System.IntPtr temp_object_value = ConvertTextInternal(x0.__Ptr);
            Text temp_object = Wrappable.Create<Text>(temp_object_value, new Text());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dConvertTexts@4")]
        private static extern System.IntPtr ConvertTextsInternal(System.IntPtr x0);

        public static Texts ConvertTexts(TextsPtr x0)
        {
            System.IntPtr temp_object_value = ConvertTextsInternal(x0.__Ptr);
            Texts temp_object = Wrappable.Create<Texts>(temp_object_value, new Texts());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewParts@0")]
        private static extern System.IntPtr NewPartsInternal();

        public static PartsPtr NewParts()
        {
            System.IntPtr temp_object_value = NewPartsInternal();
            PartsPtr temp_object = Wrappable.Create<PartsPtr>(temp_object_value, new PartsPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteParts@4")]
        private static extern void DeletePartsInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dInsertPartPtr@8")]
        private static extern void InsertPartPtrInternal(System.IntPtr x0, System.IntPtr x1);

        public static void InsertPartPtr(PartsPtr x0, PartPtr x1)
        {
            InsertPartPtrInternal(x0.__Ptr, x1.__Ptr);
            System.GC.KeepAlive(x0);
            System.GC.KeepAlive(x1);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewFaces@0")]
        private static extern System.IntPtr NewFacesInternal();

        public static FacesPtr NewFaces()
        {
            System.IntPtr temp_object_value = NewFacesInternal();
            FacesPtr temp_object = Wrappable.Create<FacesPtr>(temp_object_value, new FacesPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteFaces@4")]
        private static extern void DeleteFacesInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dInsertFacePtr@8")]
        private static extern void InsertFacePtrInternal(System.IntPtr x0, System.IntPtr x1);

        public static void InsertFacePtr(FacesPtr x0, FacePtr x1)
        {
            InsertFacePtrInternal(x0.__Ptr, x1.__Ptr);
            System.GC.KeepAlive(x0);
            System.GC.KeepAlive(x1);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewProfiles@0")]
        private static extern System.IntPtr NewProfilesInternal();

        public static ProfilesPtr NewProfiles()
        {
            System.IntPtr temp_object_value = NewProfilesInternal();
            ProfilesPtr temp_object = Wrappable.Create<ProfilesPtr>(temp_object_value, new ProfilesPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteProfiles@4")]
        private static extern void DeleteProfilesInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dInsertProfilePtr@8")]
        private static extern void InsertProfilePtrInternal(System.IntPtr x0, System.IntPtr x1);

        public static void InsertProfilePtr(ProfilesPtr x0, ProfilePtr x1)
        {
            InsertProfilePtrInternal(x0.__Ptr, x1.__Ptr);
            System.GC.KeepAlive(x0);
            System.GC.KeepAlive(x1);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewTexts@0")]
        private static extern System.IntPtr NewTextsInternal();

        public static TextsPtr NewTexts()
        {
            System.IntPtr temp_object_value = NewTextsInternal();
            TextsPtr temp_object = Wrappable.Create<TextsPtr>(temp_object_value, new TextsPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteTexts@4")]
        private static extern void DeleteTextsInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dInsertTextPtr@8")]
        private static extern void InsertTextPtrInternal(System.IntPtr texts, System.IntPtr text);

        public static void InsertTextPtr(TextsPtr texts, TextPtr text)
        {
            InsertTextPtrInternal(texts.__Ptr, text.__Ptr);
            System.GC.KeepAlive(texts);
            System.GC.KeepAlive(text);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewPartIterator@4")]
        private static extern System.IntPtr NewPartIteratorInternal(System.IntPtr x0);

        public static PartIteratorPtr NewPartIterator(Parts x0)
        {
            System.IntPtr temp_object_value = NewPartIteratorInternal(x0.__Ptr);
            PartIteratorPtr temp_object = Wrappable.Create<PartIteratorPtr>(temp_object_value, new PartIteratorPtr());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeletePartIterator@4")]
        private static extern void DeletePartIteratorInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dPartIteratorHasNext@4")]
        private static extern bool PartIteratorHasNextInternal(System.IntPtr x0);

        public static bool PartIteratorHasNext(PartIteratorPtr x0)
        {
            bool returned_value = PartIteratorHasNextInternal(x0.__Ptr);
            System.GC.KeepAlive(x0);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dPartIteratorNext@4")]
        private static extern System.IntPtr PartIteratorNextInternal(System.IntPtr x0);

        public static Part PartIteratorNext(PartIteratorPtr x0)
        {
            System.IntPtr temp_object_value = PartIteratorNextInternal(x0.__Ptr);
            Part temp_object = Wrappable.Create<Part>(temp_object_value, new Part());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewFaceIterator@4")]
        private static extern System.IntPtr NewFaceIteratorInternal(System.IntPtr x0);

        public static FaceIteratorPtr NewFaceIterator(Faces x0)
        {
            System.IntPtr temp_object_value = NewFaceIteratorInternal(x0.__Ptr);
            FaceIteratorPtr temp_object = Wrappable.Create<FaceIteratorPtr>(temp_object_value, new FaceIteratorPtr());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteFaceIterator@4")]
        private static extern void DeleteFaceIteratorInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFaceIteratorHasNext@4")]
        private static extern bool FaceIteratorHasNextInternal(System.IntPtr x0);

        public static bool FaceIteratorHasNext(FaceIteratorPtr x0)
        {
            bool returned_value = FaceIteratorHasNextInternal(x0.__Ptr);
            System.GC.KeepAlive(x0);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFaceIteratorNext@4")]
        private static extern System.IntPtr FaceIteratorNextInternal(System.IntPtr x0);

        public static Face FaceIteratorNext(FaceIteratorPtr x0)
        {
            System.IntPtr temp_object_value = FaceIteratorNextInternal(x0.__Ptr);
            Face temp_object = Wrappable.Create<Face>(temp_object_value, new Face());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewProfileIterator@4")]
        private static extern System.IntPtr NewProfileIteratorInternal(System.IntPtr x0);

        public static ProfileIteratorPtr NewProfileIterator(Profiles x0)
        {
            System.IntPtr temp_object_value = NewProfileIteratorInternal(x0.__Ptr);
            ProfileIteratorPtr temp_object = Wrappable.Create<ProfileIteratorPtr>(temp_object_value, new ProfileIteratorPtr());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteProfileIterator@4")]
        private static extern void DeleteProfileIteratorInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileIteratorHasNext@4")]
        private static extern bool ProfileIteratorHasNextInternal(System.IntPtr x0);

        public static bool ProfileIteratorHasNext(ProfileIteratorPtr x0)
        {
            bool returned_value = ProfileIteratorHasNextInternal(x0.__Ptr);
            System.GC.KeepAlive(x0);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileIteratorNext@4")]
        private static extern System.IntPtr ProfileIteratorNextInternal(System.IntPtr x0);

        public static Profile ProfileIteratorNext(ProfileIteratorPtr x0)
        {
            System.IntPtr temp_object_value = ProfileIteratorNextInternal(x0.__Ptr);
            Profile temp_object = Wrappable.Create<Profile>(temp_object_value, new Profile());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewTextIterator@4")]
        private static extern System.IntPtr NewTextIteratorInternal(System.IntPtr x0);

        public static TextIteratorPtr NewTextIterator(Texts x0)
        {
            System.IntPtr temp_object_value = NewTextIteratorInternal(x0.__Ptr);
            TextIteratorPtr temp_object = Wrappable.Create<TextIteratorPtr>(temp_object_value, new TextIteratorPtr());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteTextIterator@4")]
        private static extern void DeleteTextIteratorInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTextIteratorHasNext@4")]
        private static extern bool TextIteratorHasNextInternal(System.IntPtr x0);

        public static bool TextIteratorHasNext(TextIteratorPtr x0)
        {
            bool returned_value = TextIteratorHasNextInternal(x0.__Ptr);
            System.GC.KeepAlive(x0);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTextIteratorNext@4")]
        private static extern System.IntPtr TextIteratorNextInternal(System.IntPtr x0);

        public static Text TextIteratorNext(TextIteratorPtr x0)
        {
            System.IntPtr temp_object_value = TextIteratorNextInternal(x0.__Ptr);
            Text temp_object = Wrappable.Create<Text>(temp_object_value, new Text());
            System.GC.KeepAlive(x0);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteProfileP01@8")]
        private static extern void WriteProfileP01Internal(System.IntPtr profile, string filename);

        public static void WriteProfileP01(Profile profile, string filename)
        {
            WriteProfileP01Internal(profile.__Ptr, filename);
            System.GC.KeepAlive(profile);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteFaceP01@8")]
        private static extern void WriteFaceP01Internal(System.IntPtr face, string filename);

        public static void WriteFaceP01(Face face, string filename)
        {
            WriteFaceP01Internal(face.__Ptr, filename);
            System.GC.KeepAlive(face);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWritePartP01@8")]
        private static extern void WritePartP01Internal(System.IntPtr part, string filename);

        public static void WritePartP01(Part part, string filename)
        {
            WritePartP01Internal(part.__Ptr, filename);
            System.GC.KeepAlive(part);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadPartPtrP01@4")]
        private static extern System.IntPtr ReadPartPtrP01Internal(string filename);

        public static PartPtr ReadPartPtrP01(string filename)
        {
            System.IntPtr temp_object_value = ReadPartPtrP01Internal(filename);
            PartPtr temp_object = Wrappable.Create<PartPtr>(temp_object_value, new PartPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadProfilesPtrP01@4")]
        private static extern System.IntPtr ReadProfilesPtrP01Internal(string filename);

        public static ProfilesPtr ReadProfilesPtrP01(string filename)
        {
            System.IntPtr temp_object_value = ReadProfilesPtrP01Internal(filename);
            ProfilesPtr temp_object = Wrappable.Create<ProfilesPtr>(temp_object_value, new ProfilesPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadProfilesPtrDpr@4")]
        private static extern System.IntPtr ReadProfilesPtrDprInternal(string filename);

        public static ProfilesPtr ReadProfilesPtrDpr(string filename)
        {
            System.IntPtr temp_object_value = ReadProfilesPtrDprInternal(filename);
            ProfilesPtr temp_object = Wrappable.Create<ProfilesPtr>(temp_object_value, new ProfilesPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadTextsPtrDpr@4")]
        private static extern System.IntPtr ReadTextsPtrDprInternal(string filename);

        public static TextsPtr ReadTextsPtrDpr(string filename)
        {
            System.IntPtr temp_object_value = ReadTextsPtrDprInternal(filename);
            TextsPtr temp_object = Wrappable.Create<TextsPtr>(temp_object_value, new TextsPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadFacePtrDpr@4")]
        private static extern System.IntPtr ReadFacePtrDprInternal(string filename);

        public static FacePtr ReadFacePtrDpr(string filename)
        {
            System.IntPtr temp_object_value = ReadFacePtrDprInternal(filename);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadPartPtrDpr@4")]
        private static extern System.IntPtr ReadPartPtrDprInternal(string filename);

        public static PartPtr ReadPartPtrDpr(string filename)
        {
            System.IntPtr temp_object_value = ReadPartPtrDprInternal(filename);
            PartPtr temp_object = Wrappable.Create<PartPtr>(temp_object_value, new PartPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadCutPartPtrDpr@4")]
        private static extern System.IntPtr ReadCutPartPtrDprInternal(string filename);

        public static PartPtr ReadCutPartPtrDpr(string filename)
        {
            System.IntPtr temp_object_value = ReadCutPartPtrDprInternal(filename);
            PartPtr temp_object = Wrappable.Create<PartPtr>(temp_object_value, new PartPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadDprAttribute@4")]
        private static extern System.IntPtr ReadDprAttributeInternal(string filename);

        public static DprAttributePtr ReadDprAttribute(string filename)
        {
            System.IntPtr temp_object_value = ReadDprAttributeInternal(filename);
            DprAttributePtr temp_object = Wrappable.Create<DprAttributePtr>(temp_object_value, new DprAttributePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteProfileDpr@8")]
        private static extern void WriteProfileDprInternal(System.IntPtr profile, string filename);

        public static void WriteProfileDpr(Profile profile, string filename)
        {
            WriteProfileDprInternal(profile.__Ptr, filename);
            System.GC.KeepAlive(profile);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteProfilesDpr@8")]
        private static extern void WriteProfilesDprInternal(System.IntPtr profiles, string filename);

        public static void WriteProfilesDpr(Profiles profiles, string filename)
        {
            WriteProfilesDprInternal(profiles.__Ptr, filename);
            System.GC.KeepAlive(profiles);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteFaceDpr@8")]
        private static extern void WriteFaceDprInternal(System.IntPtr face, string filename);

        public static void WriteFaceDpr(Face face, string filename)
        {
            WriteFaceDprInternal(face.__Ptr, filename);
            System.GC.KeepAlive(face);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWritePartDpr@8")]
        private static extern void WritePartDprInternal(System.IntPtr part, string filename);

        public static void WritePartDpr(Part part, string filename)
        {
            WritePartDprInternal(part.__Ptr, filename);
            System.GC.KeepAlive(part);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterNew@4")]
        private static extern System.IntPtr DprWriterNewInternal(string filename);

        public static DprWriterPtr DprWriterNew(string filename)
        {
            System.IntPtr temp_object_value = DprWriterNewInternal(filename);
            DprWriterPtr temp_object = Wrappable.Create<DprWriterPtr>(temp_object_value, new DprWriterPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterDelete@4")]
        private static extern void DprWriterDeleteInternal(System.IntPtr writer);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterSetDprAttribute@8")]
        private static extern void DprWriterSetDprAttributeInternal(System.IntPtr writer, System.IntPtr dpr_attribute);

        public static void DprWriterSetDprAttribute(DprWriterPtr writer, DprAttributePtr dpr_attribute)
        {
            DprWriterSetDprAttributeInternal(writer.__Ptr, dpr_attribute.__Ptr);
            System.GC.KeepAlive(writer);
            System.GC.KeepAlive(dpr_attribute);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterSetOrientation@8")]
        private static extern void DprWriterSetOrientationInternal(System.IntPtr writer, bool material_left);

        public static void DprWriterSetOrientation(DprWriterPtr writer, bool material_left)
        {
            DprWriterSetOrientationInternal(writer.__Ptr, material_left);
            System.GC.KeepAlive(writer);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterWriteProfile@8")]
        private static extern int DprWriterWriteProfileInternal(System.IntPtr writer, System.IntPtr profile);

        public static int DprWriterWriteProfile(DprWriterPtr writer, Profile profile)
        {
            int returned_value = DprWriterWriteProfileInternal(writer.__Ptr, profile.__Ptr);
            System.GC.KeepAlive(writer);
            System.GC.KeepAlive(profile);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterWriteProfiles@8")]
        private static extern int DprWriterWriteProfilesInternal(System.IntPtr writer, System.IntPtr profiles);

        public static int DprWriterWriteProfiles(DprWriterPtr writer, Profiles profiles)
        {
            int returned_value = DprWriterWriteProfilesInternal(writer.__Ptr, profiles.__Ptr);
            System.GC.KeepAlive(writer);
            System.GC.KeepAlive(profiles);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterWriteFace@8")]
        private static extern int DprWriterWriteFaceInternal(System.IntPtr writer, System.IntPtr face);

        public static int DprWriterWriteFace(DprWriterPtr writer, Face face)
        {
            int returned_value = DprWriterWriteFaceInternal(writer.__Ptr, face.__Ptr);
            System.GC.KeepAlive(writer);
            System.GC.KeepAlive(face);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDprWriterWritePart@8")]
        private static extern int DprWriterWritePartInternal(System.IntPtr writer, System.IntPtr part);

        public static int DprWriterWritePart(DprWriterPtr writer, Part part)
        {
            int returned_value = DprWriterWritePartInternal(writer.__Ptr, part.__Ptr);
            System.GC.KeepAlive(writer);
            System.GC.KeepAlive(part);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWriteProfile@8")]
        private static extern void WriteProfileInternal(System.IntPtr profile, string filename);

        public static void WriteProfile(Profile profile, string filename)
        {
            WriteProfileInternal(profile.__Ptr, filename);
            System.GC.KeepAlive(profile);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadProfile@4")]
        private static extern System.IntPtr ReadProfileInternal(string filename);

        public static ProfilePtr ReadProfile(string filename)
        {
            System.IntPtr temp_object_value = ReadProfileInternal(filename);
            ProfilePtr temp_object = Wrappable.Create<ProfilePtr>(temp_object_value, new ProfilePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWritePart@8")]
        private static extern void WritePartInternal(System.IntPtr part, string filename);

        public static void WritePart(Part part, string filename)
        {
            WritePartInternal(part.__Ptr, filename);
            System.GC.KeepAlive(part);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadPart@4")]
        private static extern System.IntPtr ReadPartInternal(string filename);

        public static PartPtr ReadPart(string filename)
        {
            System.IntPtr temp_object_value = ReadPartInternal(filename);
            PartPtr temp_object = Wrappable.Create<PartPtr>(temp_object_value, new PartPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dWritePartString@16")]
        private static extern int WritePartStringInternal(System.IntPtr part, string buffer, int buffer_size, out int actual_size);

        public static int WritePartString(Part part, string buffer, int buffer_size, out int actual_size)
        {
            int returned_value = WritePartStringInternal(part.__Ptr, buffer, buffer_size, out actual_size);
            System.GC.KeepAlive(part);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dReadPartString@8")]
        private static extern System.IntPtr ReadPartStringInternal(string buffer, int buffer_size);

        public static PartPtr ReadPartString(string buffer, int buffer_size)
        {
            System.IntPtr temp_object_value = ReadPartStringInternal(buffer, buffer_size);
            PartPtr temp_object = Wrappable.Create<PartPtr>(temp_object_value, new PartPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewProfileXY@16")]
        private static extern System.IntPtr NewProfileXYInternal(double x, double y);

        public static ProfilePtr NewProfileXY(double x, double y)
        {
            System.IntPtr temp_object_value = NewProfileXYInternal(x, y);
            ProfilePtr temp_object = Wrappable.Create<ProfilePtr>(temp_object_value, new ProfilePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddSegmentXY@20")]
        private static extern System.IntPtr AddSegmentXYInternal(System.IntPtr profile, double x, double y);

        public static Edge AddSegmentXY(ProfilePtr profile, double x, double y)
        {
            System.IntPtr temp_object_value = AddSegmentXYInternal(profile.__Ptr, x, y);
            Edge temp_object = Wrappable.Create<Edge>(temp_object_value, new Edge());
            System.GC.KeepAlive(profile);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddArcXY@40")]
        private static extern System.IntPtr AddArcXYInternal(System.IntPtr profile, double x, double y, double x_center, double y_center, bool trigo);

        public static Edge AddArcXY(ProfilePtr profile, double x, double y, double x_center, double y_center, bool trigo)
        {
            System.IntPtr temp_object_value = AddArcXYInternal(profile.__Ptr, x, y, x_center, y_center, trigo);
            Edge temp_object = Wrappable.Create<Edge>(temp_object_value, new Edge());
            System.GC.KeepAlive(profile);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddArcXYArrow@28")]
        private static extern System.IntPtr AddArcXYArrowInternal(System.IntPtr profile, double x, double y, double arrow);

        public static Edge AddArcXYArrow(ProfilePtr profile, double x, double y, double arrow)
        {
            System.IntPtr temp_object_value = AddArcXYArrowInternal(profile.__Ptr, x, y, arrow);
            Edge temp_object = Wrappable.Create<Edge>(temp_object_value, new Edge());
            System.GC.KeepAlive(profile);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetSegmentXY@20")]
        private static extern void GetSegmentXYInternal(System.IntPtr edge, out double start_x, out double start_y, out double end_x, out double end_y);

        public static void GetSegmentXY(Edge edge, out double start_x, out double start_y, out double end_x, out double end_y)
        {
            GetSegmentXYInternal(edge.__Ptr, out start_x, out start_y, out end_x, out end_y);
            System.GC.KeepAlive(edge);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetArcXY@32")]
        private static extern void GetArcXYInternal(System.IntPtr edge, out double start_x, out double start_y, out double end_x, out double end_y, out double center_x, out double center_y, out bool trigo);

        public static void GetArcXY(Edge edge, out double start_x, out double start_y, out double end_x, out double end_y, out double center_x, out double center_y, out bool trigo)
        {
            GetArcXYInternal(edge.__Ptr, out start_x, out start_y, out end_x, out end_y, out center_x, out center_y, out trigo);
            System.GC.KeepAlive(edge);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetPartBoundingBox@20")]
        private static extern void GetPartBoundingBoxInternal(System.IntPtr part, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y);

        public static void GetPartBoundingBox(Part part, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y)
        {
            GetPartBoundingBoxInternal(part.__Ptr, out bottom_left_x, out bottom_left_y, out top_right_x, out top_right_y);
            System.GC.KeepAlive(part);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFaceBoundingBox@20")]
        private static extern void GetFaceBoundingBoxInternal(System.IntPtr face, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y);

        public static void GetFaceBoundingBox(Face face, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y)
        {
            GetFaceBoundingBoxInternal(face.__Ptr, out bottom_left_x, out bottom_left_y, out top_right_x, out top_right_y);
            System.GC.KeepAlive(face);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetProfileBoundingBox@20")]
        private static extern void GetProfileBoundingBoxInternal(System.IntPtr profile, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y);

        public static void GetProfileBoundingBox(Profile profile, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y)
        {
            GetProfileBoundingBoxInternal(profile.__Ptr, out bottom_left_x, out bottom_left_y, out top_right_x, out top_right_y);
            System.GC.KeepAlive(profile);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetProfilesBoundingBox@20")]
        private static extern void GetProfilesBoundingBoxInternal(System.IntPtr profile, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y);

        public static void GetProfilesBoundingBox(Profiles profile, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y)
        {
            GetProfilesBoundingBoxInternal(profile.__Ptr, out bottom_left_x, out bottom_left_y, out top_right_x, out top_right_y);
            System.GC.KeepAlive(profile);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetTextBoundingBox@20")]
        private static extern void GetTextBoundingBoxInternal(System.IntPtr text, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y);

        public static void GetTextBoundingBox(Text text, out double bottom_left_x, out double bottom_left_y, out double top_right_x, out double top_right_y)
        {
            GetTextBoundingBoxInternal(text.__Ptr, out bottom_left_x, out bottom_left_y, out top_right_x, out top_right_y);
            System.GC.KeepAlive(text);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFaceMinSize@12")]
        private static extern void GetFaceMinSizeInternal(System.IntPtr face, out double length, out double width);

        public static void GetFaceMinSize(Face face, out double length, out double width)
        {
            GetFaceMinSizeInternal(face.__Ptr, out length, out width);
            System.GC.KeepAlive(face);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFaceArea@4")]
        private static extern double GetFaceAreaInternal(System.IntPtr face);

        public static double GetFaceArea(Face face)
        {
            double returned_value = GetFaceAreaInternal(face.__Ptr);
            System.GC.KeepAlive(face);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetPartArea@4")]
        private static extern double GetPartAreaInternal(System.IntPtr part);

        public static double GetPartArea(Part part)
        {
            double returned_value = GetPartAreaInternal(part.__Ptr);
            System.GC.KeepAlive(part);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetProfileLength@4")]
        private static extern double GetProfileLengthInternal(System.IntPtr profile);

        public static double GetProfileLength(Profile profile)
        {
            double returned_value = GetProfileLengthInternal(profile.__Ptr);
            System.GC.KeepAlive(profile);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetProfileIsClosed@4")]
        private static extern bool GetProfileIsClosedInternal(System.IntPtr profile);

        public static bool GetProfileIsClosed(Profile profile)
        {
            bool returned_value = GetProfileIsClosedInternal(profile.__Ptr);
            System.GC.KeepAlive(profile);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetProfileArea@4")]
        private static extern double GetProfileAreaInternal(System.IntPtr profile);

        public static double GetProfileArea(Profile profile)
        {
            double returned_value = GetProfileAreaInternal(profile.__Ptr);
            System.GC.KeepAlive(profile);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dIsProfileG1@4")]
        private static extern bool IsProfileG1Internal(System.IntPtr profile);

        public static bool IsProfileG1(Profile profile)
        {
            bool returned_value = IsProfileG1Internal(profile.__Ptr);
            System.GC.KeepAlive(profile);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetEdgeLength@4")]
        private static extern double GetEdgeLengthInternal(System.IntPtr edge);

        public static double GetEdgeLength(Edge edge)
        {
            double returned_value = GetEdgeLengthInternal(edge.__Ptr);
            System.GC.KeepAlive(edge);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dIdentifyPartWithRectangularExternal@4")]
        private static extern bool IdentifyPartWithRectangularExternalInternal(System.IntPtr part);

        public static bool IdentifyPartWithRectangularExternal(Part part)
        {
            bool returned_value = IdentifyPartWithRectangularExternalInternal(part.__Ptr);
            System.GC.KeepAlive(part);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRotatePart@28")]
        private static extern void RotatePartInternal(System.IntPtr part, double angle, double pivot_x, double pivot_y);

        public static void RotatePart(PartPtr part, double angle, double pivot_x, double pivot_y)
        {
            RotatePartInternal(part.__Ptr, angle, pivot_x, pivot_y);
            System.GC.KeepAlive(part);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRotateFace@28")]
        private static extern void RotateFaceInternal(System.IntPtr face, double angle, double pivot_x, double pivot_y);

        public static void RotateFace(FacePtr face, double angle, double pivot_x, double pivot_y)
        {
            RotateFaceInternal(face.__Ptr, angle, pivot_x, pivot_y);
            System.GC.KeepAlive(face);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRotateProfile@28")]
        private static extern void RotateProfileInternal(System.IntPtr profile, double angle, double pivot_x, double pivot_y);

        public static void RotateProfile(ProfilePtr profile, double angle, double pivot_x, double pivot_y)
        {
            RotateProfileInternal(profile.__Ptr, angle, pivot_x, pivot_y);
            System.GC.KeepAlive(profile);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRotateText@28")]
        private static extern void RotateTextInternal(System.IntPtr text, double angle, double pivot_x, double pivot_y);

        public static void RotateText(TextPtr text, double angle, double pivot_x, double pivot_y)
        {
            RotateTextInternal(text.__Ptr, angle, pivot_x, pivot_y);
            System.GC.KeepAlive(text);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTranslatePart@20")]
        private static extern void TranslatePartInternal(System.IntPtr part, double x_move, double y_move);

        public static void TranslatePart(PartPtr part, double x_move, double y_move)
        {
            TranslatePartInternal(part.__Ptr, x_move, y_move);
            System.GC.KeepAlive(part);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTranslateFace@20")]
        private static extern void TranslateFaceInternal(System.IntPtr face, double x_move, double y_move);

        public static void TranslateFace(FacePtr face, double x_move, double y_move)
        {
            TranslateFaceInternal(face.__Ptr, x_move, y_move);
            System.GC.KeepAlive(face);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTranslateProfile@20")]
        private static extern void TranslateProfileInternal(System.IntPtr profile, double x_move, double y_move);

        public static void TranslateProfile(ProfilePtr profile, double x_move, double y_move)
        {
            TranslateProfileInternal(profile.__Ptr, x_move, y_move);
            System.GC.KeepAlive(profile);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTranslateText@20")]
        private static extern void TranslateTextInternal(System.IntPtr text, double x_move, double y_move);

        public static void TranslateText(TextPtr text, double x_move, double y_move)
        {
            TranslateTextInternal(text.__Ptr, x_move, y_move);
            System.GC.KeepAlive(text);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSymmetrizePart@36")]
        private static extern void SymmetrizePartInternal(System.IntPtr part, double x_base, double y_base, double x_vector, double y_vector);

        public static void SymmetrizePart(PartPtr part, double x_base, double y_base, double x_vector, double y_vector)
        {
            SymmetrizePartInternal(part.__Ptr, x_base, y_base, x_vector, y_vector);
            System.GC.KeepAlive(part);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSymmetrizeFace@36")]
        private static extern void SymmetrizeFaceInternal(System.IntPtr face, double x_base, double y_base, double x_vector, double y_vector);

        public static void SymmetrizeFace(FacePtr face, double x_base, double y_base, double x_vector, double y_vector)
        {
            SymmetrizeFaceInternal(face.__Ptr, x_base, y_base, x_vector, y_vector);
            System.GC.KeepAlive(face);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSymmetrizeProfile@36")]
        private static extern void SymmetrizeProfileInternal(System.IntPtr profile, double x_base, double y_base, double x_vector, double y_vector);

        public static void SymmetrizeProfile(ProfilePtr profile, double x_base, double y_base, double x_vector, double y_vector)
        {
            SymmetrizeProfileInternal(profile.__Ptr, x_base, y_base, x_vector, y_vector);
            System.GC.KeepAlive(profile);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSymmetrizeText@36")]
        private static extern void SymmetrizeTextInternal(System.IntPtr text, double x_base, double y_base, double x_vector, double y_vector);

        public static void SymmetrizeText(TextPtr text, double x_base, double y_base, double x_vector, double y_vector)
        {
            SymmetrizeTextInternal(text.__Ptr, x_base, y_base, x_vector, y_vector);
            System.GC.KeepAlive(text);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTranslateBBBottomLeftToOrigin@4")]
        private static extern void TranslateBBBottomLeftToOriginInternal(System.IntPtr profile);

        public static void TranslateBBBottomLeftToOrigin(ProfilePtr profile)
        {
            TranslateBBBottomLeftToOriginInternal(profile.__Ptr);
            System.GC.KeepAlive(profile);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileCornerBottomLeft@4")]
        private static extern int ProfileCornerBottomLeftInternal(System.IntPtr profile);

        public static int ProfileCornerBottomLeft(ProfilePtr profile)
        {
            int returned_value = ProfileCornerBottomLeftInternal(profile.__Ptr);
            System.GC.KeepAlive(profile);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileRightAngleAtCorner@8")]
        private static extern int ProfileRightAngleAtCornerInternal(System.IntPtr profile, int wanted_corner);

        public static int ProfileRightAngleAtCorner(ProfilePtr profile, int wanted_corner)
        {
            int returned_value = ProfileRightAngleAtCornerInternal(profile.__Ptr, wanted_corner);
            System.GC.KeepAlive(profile);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFilletXY@28")]
        private static extern bool FilletXYInternal(System.IntPtr profile, double x, double y, double radius);

        public static bool FilletXY(ProfilePtr profile, double x, double y, double radius)
        {
            bool returned_value = FilletXYInternal(profile.__Ptr, x, y, radius);
            System.GC.KeepAlive(profile);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFilletAll@28")]
        private static extern void FilletAllInternal(System.IntPtr profile, double radius, double minThreshold, double maxThreshold);

        public static void FilletAll(ProfilePtr profile, double radius, double minThreshold, double maxThreshold)
        {
            FilletAllInternal(profile.__Ptr, radius, minThreshold, maxThreshold);
            System.GC.KeepAlive(profile);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dIsInsideProfile@32")]
        private static extern int IsInsideProfileInternal(System.IntPtr profile, double point_x, double point_y, int boundary_accepted, double max_distance);

        public static int IsInsideProfile(ProfilePtr profile, double point_x, double point_y, int boundary_accepted, double max_distance)
        {
            int returned_value = IsInsideProfileInternal(profile.__Ptr, point_x, point_y, boundary_accepted, max_distance);
            System.GC.KeepAlive(profile);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dIsInsideFace@36")]
        private static extern int IsInsideFaceInternal(System.IntPtr face, double point_x, double point_y, int external_boundary_accepted, int internal_boundary_accepted, double max_distance);

        public static int IsInsideFace(FacePtr face, double point_x, double point_y, int external_boundary_accepted, int internal_boundary_accepted, double max_distance)
        {
            int returned_value = IsInsideFaceInternal(face.__Ptr, point_x, point_y, external_boundary_accepted, internal_boundary_accepted, max_distance);
            System.GC.KeepAlive(face);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfilesIntersect@8")]
        private static extern bool ProfilesIntersectInternal(System.IntPtr profile1, System.IntPtr profile2);

        public static bool ProfilesIntersect(ProfilePtr profile1, ProfilePtr profile2)
        {
            bool returned_value = ProfilesIntersectInternal(profile1.__Ptr, profile2.__Ptr);
            System.GC.KeepAlive(profile1);
            System.GC.KeepAlive(profile2);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileSplitOnIntersections@4")]
        private static extern System.IntPtr ProfileSplitOnIntersectionsInternal(System.IntPtr profile);

        public static ProfilesPtr ProfileSplitOnIntersections(ProfilePtr profile)
        {
            System.IntPtr temp_object_value = ProfileSplitOnIntersectionsInternal(profile.__Ptr);
            ProfilesPtr temp_object = Wrappable.Create<ProfilesPtr>(temp_object_value, new ProfilesPtr());
            System.GC.KeepAlive(profile);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGridCutting@40")]
        private static extern System.IntPtr GridCuttingInternal(System.IntPtr part, double dx, double dy, double leadin_distance, double border_distance, bool smallest_side_first);

        public static ProfilesPtr GridCutting(Part part, double dx, double dy, double leadin_distance, double border_distance, bool smallest_side_first)
        {
            System.IntPtr temp_object_value = GridCuttingInternal(part.__Ptr, dx, dy, leadin_distance, border_distance, smallest_side_first);
            ProfilesPtr temp_object = Wrappable.Create<ProfilesPtr>(temp_object_value, new ProfilesPtr());
            System.GC.KeepAlive(part);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGridCuttingMinLengthLines@40")]
        private static extern System.IntPtr GridCuttingMinLengthLinesInternal(System.IntPtr part, double dx, double dy, double leadin_distance, double border_distance, bool smallest_side_first);

        public static ProfilesPtr GridCuttingMinLengthLines(Part part, double dx, double dy, double leadin_distance, double border_distance, bool smallest_side_first)
        {
            System.IntPtr temp_object_value = GridCuttingMinLengthLinesInternal(part.__Ptr, dx, dy, leadin_distance, border_distance, smallest_side_first);
            ProfilesPtr temp_object = Wrappable.Create<ProfilesPtr>(temp_object_value, new ProfilesPtr());
            System.GC.KeepAlive(part);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dProfileFindArcs@12")]
        private static extern void ProfileFindArcsInternal(System.IntPtr profile, double epsilon);

        public static void ProfileFindArcs(ProfilePtr profile, double epsilon)
        {
            ProfileFindArcsInternal(profile.__Ptr, epsilon);
            System.GC.KeepAlive(profile);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeCircleFaceXY@8")]
        private static extern System.IntPtr MakeCircleFaceXYInternal(double radius);

        public static FacePtr MakeCircleFaceXY(double radius)
        {
            System.IntPtr temp_object_value = MakeCircleFaceXYInternal(radius);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakePolygonEllipseFace@20")]
        private static extern System.IntPtr MakePolygonEllipseFaceInternal(double big_radius, double small_radius, int nb_sides);

        public static FacePtr MakePolygonEllipseFace(double big_radius, double small_radius, int nb_sides)
        {
            System.IntPtr temp_object_value = MakePolygonEllipseFaceInternal(big_radius, small_radius, nb_sides);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeArcsEllipseProfile@20")]
        private static extern System.IntPtr MakeArcsEllipseProfileInternal(double big_radius, double small_radius, int arcs_number);

        public static ProfilePtr MakeArcsEllipseProfile(double big_radius, double small_radius, int arcs_number)
        {
            System.IntPtr temp_object_value = MakeArcsEllipseProfileInternal(big_radius, small_radius, arcs_number);
            ProfilePtr temp_object = Wrappable.Create<ProfilePtr>(temp_object_value, new ProfilePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeCircleFaceWithPositionedCircleHoleXY@24")]
        private static extern System.IntPtr MakeCircleFaceWithPositionedCircleHoleXYInternal(double radius_ext, double radius_int, double offset);

        public static FacePtr MakeCircleFaceWithPositionedCircleHoleXY(double radius_ext, double radius_int, double offset)
        {
            System.IntPtr temp_object_value = MakeCircleFaceWithPositionedCircleHoleXYInternal(radius_ext, radius_int, offset);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeCircleFaceWithCenteredRingOfHoleXY@36")]
        private static extern System.IntPtr MakeCircleFaceWithCenteredRingOfHoleXYInternal(double radius_ext, double radius_ring, double radius_hole, uint N, double radius_int);

        public static FacePtr MakeCircleFaceWithCenteredRingOfHoleXY(double radius_ext, double radius_ring, double radius_hole, uint N, double radius_int)
        {
            System.IntPtr temp_object_value = MakeCircleFaceWithCenteredRingOfHoleXYInternal(radius_ext, radius_ring, radius_hole, N, radius_int);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeCircleFaceWith2CenteredRingsOfHoleXY@80")]
        private static extern System.IntPtr MakeCircleFaceWith2CenteredRingsOfHoleXYInternal(double radius_ext, double radius_int, double radius_ring1, double radius_hole1, double theta, uint N1, double radius_ring2, double radius_hole2, double angle_start2, double angle_move2, uint N2);

        public static FacePtr MakeCircleFaceWith2CenteredRingsOfHoleXY(double radius_ext, double radius_int, double radius_ring1, double radius_hole1, double theta, uint N1, double radius_ring2, double radius_hole2, double angle_start2, double angle_move2, uint N2)
        {
            System.IntPtr temp_object_value = MakeCircleFaceWith2CenteredRingsOfHoleXYInternal(radius_ext, radius_int, radius_ring1, radius_hole1, theta, N1, radius_ring2, radius_hole2, angle_start2, angle_move2, N2);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeBrokenRingFaceWith2CenteredRingsOfHoleXY@96")]
        private static extern System.IntPtr MakeBrokenRingFaceWith2CenteredRingsOfHoleXYInternal(double radius_ext, double radius_int, double theta, double radius_ring1, double radius_hole1, double angle_start1, double angle_move1, uint N1, double radius_ring2, double radius_hole2, double angle_start2, double angle_move2, uint N2);

        public static FacePtr MakeBrokenRingFaceWith2CenteredRingsOfHoleXY(double radius_ext, double radius_int, double theta, double radius_ring1, double radius_hole1, double angle_start1, double angle_move1, uint N1, double radius_ring2, double radius_hole2, double angle_start2, double angle_move2, uint N2)
        {
            System.IntPtr temp_object_value = MakeBrokenRingFaceWith2CenteredRingsOfHoleXYInternal(radius_ext, radius_int, theta, radius_ring1, radius_hole1, angle_start1, angle_move1, N1, radius_ring2, radius_hole2, angle_start2, angle_move2, N2);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeBrokenRingFaceXY@24")]
        private static extern System.IntPtr MakeBrokenRingFaceXYInternal(double radius_ext, double radius_int, double theta);

        public static FacePtr MakeBrokenRingFaceXY(double radius_ext, double radius_int, double theta)
        {
            System.IntPtr temp_object_value = MakeBrokenRingFaceXYInternal(radius_ext, radius_int, theta);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeTriangleFaceXY@48")]
        private static extern System.IntPtr MakeTriangleFaceXYInternal(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y);

        public static FacePtr MakeTriangleFaceXY(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y)
        {
            System.IntPtr temp_object_value = MakeTriangleFaceXYInternal(p1_x, p1_y, p2_x, p2_y, p3_x, p3_y);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleProfileXY@32")]
        private static extern System.IntPtr MakeRectangleProfileXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y);

        public static ProfilePtr MakeRectangleProfileXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y)
        {
            System.IntPtr temp_object_value = MakeRectangleProfileXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y);
            ProfilePtr temp_object = Wrappable.Create<ProfilePtr>(temp_object_value, new ProfilePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceXY@32")]
        private static extern System.IntPtr MakeRectangleFaceXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y);

        public static FacePtr MakeRectangleFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithHoleXY@40")]
        private static extern System.IntPtr MakeRectangleFaceWithHoleXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double radius);

        public static FacePtr MakeRectangleFaceWithHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double radius)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithHoleXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, radius);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithPositionedRectangleHoleXY@64")]
        private static extern System.IntPtr MakeRectangleFaceWithPositionedRectangleHoleXYInternal(double ext_bottom_left_x, double ext_bottom_left_y, double ext_top_right_x, double ext_top_right_y, double int_bottom_left_x, double int_bottom_left_y, double int_top_right_x, double int_top_right_y);

        public static FacePtr MakeRectangleFaceWithPositionedRectangleHoleXY(double ext_bottom_left_x, double ext_bottom_left_y, double ext_top_right_x, double ext_top_right_y, double int_bottom_left_x, double int_bottom_left_y, double int_top_right_x, double int_top_right_y)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithPositionedRectangleHoleXYInternal(ext_bottom_left_x, ext_bottom_left_y, ext_top_right_x, ext_top_right_y, int_bottom_left_x, int_bottom_left_y, int_top_right_x, int_top_right_y);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithGridHoleXY@56")]
        private static extern System.IntPtr MakeRectangleFaceWithGridHoleXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, double dy, double radius);

        public static FacePtr MakeRectangleFaceWithGridHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, double dy, double radius)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithGridHoleXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, dx, dy, radius);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithGridNMHoleXY@64")]
        private static extern System.IntPtr MakeRectangleFaceWithGridNMHoleXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, double dy, int nX, int nY, double radius);

        public static FacePtr MakeRectangleFaceWithGridNMHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, double dy, int nX, int nY, double radius)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithGridNMHoleXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, dx, dy, nX, nY, radius);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithGridNMHoleOffsetXY@80")]
        private static extern System.IntPtr MakeRectangleFaceWithGridNMHoleOffsetXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double offset_x, double offset_y, double dX, double dY, int nX, int nY, double radius);

        public static FacePtr MakeRectangleFaceWithGridNMHoleOffsetXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double offset_x, double offset_y, double dX, double dY, int nX, int nY, double radius)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithGridNMHoleOffsetXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, offset_x, offset_y, dX, dY, nX, nY, radius);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithOblongsAlignXY@56")]
        private static extern System.IntPtr MakeRectangleFaceWithOblongsAlignXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, double length, double radius);

        public static FacePtr MakeRectangleFaceWithOblongsAlignXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, double length, double radius)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithOblongsAlignXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, dx, length, radius);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithNOblongsAlignXY@60")]
        private static extern System.IntPtr MakeRectangleFaceWithNOblongsAlignXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, int nX, double length, double radius);

        public static FacePtr MakeRectangleFaceWithNOblongsAlignXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double dx, int nX, double length, double radius)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithNOblongsAlignXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, dx, nX, length, radius);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectangleFaceWithNOblongsAlignOffsetXY@76")]
        private static extern System.IntPtr MakeRectangleFaceWithNOblongsAlignOffsetXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double offset_x, double offset_y, double dX, int nX, double length, double radius);

        public static FacePtr MakeRectangleFaceWithNOblongsAlignOffsetXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double offset_x, double offset_y, double dX, int nX, double length, double radius)
        {
            System.IntPtr temp_object_value = MakeRectangleFaceWithNOblongsAlignOffsetXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, offset_x, offset_y, dX, nX, length, radius);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRoundRectangleFaceXY@40")]
        private static extern System.IntPtr MakeRoundRectangleFaceXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value);

        public static FacePtr MakeRoundRectangleFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value)
        {
            System.IntPtr temp_object_value = MakeRoundRectangleFaceXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, round_value);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRoundRectangleFaceWithHoleXY@48")]
        private static extern System.IntPtr MakeRoundRectangleFaceWithHoleXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double radius);

        public static FacePtr MakeRoundRectangleFaceWithHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double radius)
        {
            System.IntPtr temp_object_value = MakeRoundRectangleFaceWithHoleXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, round_value, radius);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRoundRectangleFaceWithOblongXY@56")]
        private static extern System.IntPtr MakeRoundRectangleFaceWithOblongXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double oblong_length, double oblong_radius);

        public static FacePtr MakeRoundRectangleFaceWithOblongXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double oblong_length, double oblong_radius)
        {
            System.IntPtr temp_object_value = MakeRoundRectangleFaceWithOblongXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, round_value, oblong_length, oblong_radius);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRoundRectangleFaceWithFiveHolesXY@72")]
        private static extern System.IntPtr MakeRoundRectangleFaceWithFiveHolesXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double radiusCorners, double radiusCenterHole, double dX, double dY, double radiusHolesInCorners);

        public static FacePtr MakeRoundRectangleFaceWithFiveHolesXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double radiusCorners, double radiusCenterHole, double dX, double dY, double radiusHolesInCorners)
        {
            System.IntPtr temp_object_value = MakeRoundRectangleFaceWithFiveHolesXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, radiusCorners, radiusCenterHole, dX, dY, radiusHolesInCorners);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeEatenRectangleFaceXY@40")]
        private static extern System.IntPtr MakeEatenRectangleFaceXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value);

        public static FacePtr MakeEatenRectangleFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value)
        {
            System.IntPtr temp_object_value = MakeEatenRectangleFaceXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, round_value);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeEatenRectangleFaceWithHoleXY@48")]
        private static extern System.IntPtr MakeEatenRectangleFaceWithHoleXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double radius);

        public static FacePtr MakeEatenRectangleFaceWithHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double radius)
        {
            System.IntPtr temp_object_value = MakeEatenRectangleFaceWithHoleXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, round_value, radius);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeEatenRectangleFaceWithOblongXY@56")]
        private static extern System.IntPtr MakeEatenRectangleFaceWithOblongXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double oblong_length, double oblong_radius);

        public static FacePtr MakeEatenRectangleFaceWithOblongXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double round_value, double oblong_length, double oblong_radius)
        {
            System.IntPtr temp_object_value = MakeEatenRectangleFaceWithOblongXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, round_value, oblong_length, oblong_radius);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeChamferRectangleFaceXY@40")]
        private static extern System.IntPtr MakeChamferRectangleFaceXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double chamfer_value);

        public static FacePtr MakeChamferRectangleFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double chamfer_value)
        {
            System.IntPtr temp_object_value = MakeChamferRectangleFaceXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, chamfer_value);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeEatenTrapezoidXY@80")]
        private static extern System.IntPtr MakeEatenTrapezoidXYInternal(double bottom_left_x, double bottom_left_y, double bottom_right_x, double bottom_right_y, double top_left_x, double top_left_y, double top_right_x, double top_right_y, double top_offset, double radius);

        public static FacePtr MakeEatenTrapezoidXY(double bottom_left_x, double bottom_left_y, double bottom_right_x, double bottom_right_y, double top_left_x, double top_left_y, double top_right_x, double top_right_y, double top_offset, double radius)
        {
            System.IntPtr temp_object_value = MakeEatenTrapezoidXYInternal(bottom_left_x, bottom_left_y, bottom_right_x, bottom_right_y, top_left_x, top_left_y, top_right_x, top_right_y, top_offset, radius);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeChamferRectangleFaceWithHoleXY@48")]
        private static extern System.IntPtr MakeChamferRectangleFaceWithHoleXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double chamfer_value, double radius);

        public static FacePtr MakeChamferRectangleFaceWithHoleXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double chamfer_value, double radius)
        {
            System.IntPtr temp_object_value = MakeChamferRectangleFaceWithHoleXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, chamfer_value, radius);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeChamferRectangleFaceWithOblongXY@56")]
        private static extern System.IntPtr MakeChamferRectangleFaceWithOblongXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double chamfer_value, double oblong_length, double oblong_radius);

        public static FacePtr MakeChamferRectangleFaceWithOblongXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double chamfer_value, double oblong_length, double oblong_radius)
        {
            System.IntPtr temp_object_value = MakeChamferRectangleFaceWithOblongXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, chamfer_value, oblong_length, oblong_radius);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeOblongFaceXY@32")]
        private static extern System.IntPtr MakeOblongFaceXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y);

        public static FacePtr MakeOblongFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y)
        {
            System.IntPtr temp_object_value = MakeOblongFaceXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeLFaceXY@48")]
        private static extern System.IntPtr MakeLFaceXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double thicknessH, double thicknessV);

        public static FacePtr MakeLFaceXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y, double thicknessH, double thicknessV)
        {
            System.IntPtr temp_object_value = MakeLFaceXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y, thicknessH, thicknessV);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeTFaceXY@32")]
        private static extern System.IntPtr MakeTFaceXYInternal(double length, double width, double thicknessH, double thicknessV);

        public static FacePtr MakeTFaceXY(double length, double width, double thicknessH, double thicknessV)
        {
            System.IntPtr temp_object_value = MakeTFaceXYInternal(length, width, thicknessH, thicknessV);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeCamXY@56")]
        private static extern System.IntPtr MakeCamXYInternal(double spacing_ext, double radius_ext_left, double radius_ext_right, double pos_int_left, double radius_int_left, double pos_int_right, double radius_int_right);

        public static FacePtr MakeCamXY(double spacing_ext, double radius_ext_left, double radius_ext_right, double pos_int_left, double radius_int_left, double pos_int_right, double radius_int_right)
        {
            System.IntPtr temp_object_value = MakeCamXYInternal(spacing_ext, radius_ext_left, radius_ext_right, pos_int_left, radius_int_left, pos_int_right, radius_int_right);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeKeyXY@48")]
        private static extern System.IntPtr MakeKeyXYInternal(double spacing, double radius_ext, double radius_int, double radius, double thickness, double radius_round);

        public static FacePtr MakeKeyXY(double spacing, double radius_ext, double radius_int, double radius, double thickness, double radius_round)
        {
            System.IntPtr temp_object_value = MakeKeyXYInternal(spacing, radius_ext, radius_int, radius, thickness, radius_round);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRodXY@56")]
        private static extern System.IntPtr MakeRodXYInternal(double spacing, double radius_ext_left, double radius_ext_right, double radius_int_left, double radius_int_right, double thickness, double radius_round);

        public static FacePtr MakeRodXY(double spacing, double radius_ext_left, double radius_ext_right, double radius_int_left, double radius_int_right, double thickness, double radius_round)
        {
            System.IntPtr temp_object_value = MakeRodXYInternal(spacing, radius_ext_left, radius_ext_right, radius_int_left, radius_int_right, thickness, radius_round);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakePolygone5FaceXY@80")]
        private static extern System.IntPtr MakePolygone5FaceXYInternal(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y, double p4_x, double p4_y, double p5_x, double p5_y);

        public static FacePtr MakePolygone5FaceXY(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y, double p4_x, double p4_y, double p5_x, double p5_y)
        {
            System.IntPtr temp_object_value = MakePolygone5FaceXYInternal(p1_x, p1_y, p2_x, p2_y, p3_x, p3_y, p4_x, p4_y, p5_x, p5_y);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakePolygone6FaceXY@96")]
        private static extern System.IntPtr MakePolygone6FaceXYInternal(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y, double p4_x, double p4_y, double p5_x, double p5_y, double p6_x, double p6_y);

        public static FacePtr MakePolygone6FaceXY(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y, double p4_x, double p4_y, double p5_x, double p5_y, double p6_x, double p6_y)
        {
            System.IntPtr temp_object_value = MakePolygone6FaceXYInternal(p1_x, p1_y, p2_x, p2_y, p3_x, p3_y, p4_x, p4_y, p5_x, p5_y, p6_x, p6_y);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakePolygone8FaceXY@128")]
        private static extern System.IntPtr MakePolygone8FaceXYInternal(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y, double p4_x, double p4_y, double p5_x, double p5_y, double p6_x, double p6_y, double p7_x, double p7_y, double p8_x, double p8_y);

        public static FacePtr MakePolygone8FaceXY(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y, double p4_x, double p4_y, double p5_x, double p5_y, double p6_x, double p6_y, double p7_x, double p7_y, double p8_x, double p8_y)
        {
            System.IntPtr temp_object_value = MakePolygone8FaceXYInternal(p1_x, p1_y, p2_x, p2_y, p3_x, p3_y, p4_x, p4_y, p5_x, p5_y, p6_x, p6_y, p7_x, p7_y, p8_x, p8_y);
            FacePtr temp_object = Wrappable.Create<FacePtr>(temp_object_value, new FacePtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dMakeRectanglePartXY@32")]
        private static extern System.IntPtr MakeRectanglePartXYInternal(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y);

        public static PartPtr MakeRectanglePartXY(double bottom_left_x, double bottom_left_y, double top_right_x, double top_right_y)
        {
            System.IntPtr temp_object_value = MakeRectanglePartXYInternal(bottom_left_x, bottom_left_y, top_right_x, top_right_y);
            PartPtr temp_object = Wrappable.Create<PartPtr>(temp_object_value, new PartPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dFaceHasIntersections@4")]
        private static extern bool FaceHasIntersectionsInternal(System.IntPtr face);

        public static bool FaceHasIntersections(FacePtr face)
        {
            bool returned_value = FaceHasIntersectionsInternal(face.__Ptr);
            System.GC.KeepAlive(face);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewSimpleText@8")]
        private static extern System.IntPtr NewSimpleTextInternal(string text, string font);

        public static TextPtr NewSimpleText(string text, string font)
        {
            System.IntPtr temp_object_value = NewSimpleTextInternal(text, font);
            TextPtr temp_object = Wrappable.Create<TextPtr>(temp_object_value, new TextPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dNewTextXY@40")]
        private static extern System.IntPtr NewTextXYInternal(string text, double x, double y, double angle, string font, double font_height);

        public static TextPtr NewTextXY(string text, double x, double y, double angle, string font, double font_height)
        {
            System.IntPtr temp_object_value = NewTextXYInternal(text, x, y, angle, font, font_height);
            TextPtr temp_object = Wrappable.Create<TextPtr>(temp_object_value, new TextPtr());
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dDeleteText@4")]
        private static extern void DeleteTextInternal(System.IntPtr x0);

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dAddTextToFace@8")]
        private static extern void AddTextToFaceInternal(System.IntPtr face, System.IntPtr text);

        public static void AddTextToFace(Face face, TextPtr text)
        {
            AddTextToFaceInternal(face.__Ptr, text.__Ptr);
            System.GC.KeepAlive(face);
            System.GC.KeepAlive(text);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetTextString@12")]
        private static extern void GetTextStringInternal(System.IntPtr text, System.Text.StringBuilder out_string, int string_size);

        public static void GetTextString(Text text, System.Text.StringBuilder out_string, int string_size)
        {
            GetTextStringInternal(text.__Ptr, out_string, string_size);
            System.GC.KeepAlive(text);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTextGetAngle@4")]
        private static extern double TextGetAngleInternal(System.IntPtr text);

        public static double TextGetAngle(Text text)
        {
            double returned_value = TextGetAngleInternal(text.__Ptr);
            System.GC.KeepAlive(text);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTextGetHeight@4")]
        private static extern double TextGetHeightInternal(System.IntPtr text);

        public static double TextGetHeight(Text text)
        {
            double returned_value = TextGetHeightInternal(text.__Ptr);
            System.GC.KeepAlive(text);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dTextGetPoint@12")]
        private static extern void TextGetPointInternal(System.IntPtr text, out double x, out double y);

        public static void TextGetPoint(Text text, out double x, out double y)
        {
            TextGetPointInternal(text.__Ptr, out x, out y);
            System.GC.KeepAlive(text);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetTextFont@12")]
        private static extern void GetTextFontInternal(System.IntPtr text, System.Text.StringBuilder out_font_name, int font_name_size);

        public static void GetTextFont(Text text, System.Text.StringBuilder out_font_name, int font_name_size)
        {
            GetTextFontInternal(text.__Ptr, out_font_name, font_name_size);
            System.GC.KeepAlive(text);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetTextHeight@12")]
        private static extern void SetTextHeightInternal(System.IntPtr text, double height);

        public static void SetTextHeight(Text text, double height)
        {
            SetTextHeightInternal(text.__Ptr, height);
            System.GC.KeepAlive(text);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetTextAttachmentType@8")]
        private static extern void SetTextAttachmentTypeInternal(System.IntPtr text, AttachmentType attachment_type);

        public static void SetTextAttachmentType(Text text, AttachmentType attachment_type)
        {
            SetTextAttachmentTypeInternal(text.__Ptr, attachment_type);
            System.GC.KeepAlive(text);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRenderText@4")]
        private static extern System.IntPtr RenderTextInternal(System.IntPtr text);

        public static PartPtr RenderText(Text text)
        {
            System.IntPtr temp_object_value = RenderTextInternal(text.__Ptr);
            PartPtr temp_object = Wrappable.Create<PartPtr>(temp_object_value, new PartPtr());
            System.GC.KeepAlive(text);
            return temp_object;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetMachiningOnProfile@8")]
        private static extern void SetMachiningOnProfileInternal(System.IntPtr profile, int machining);

        public static void SetMachiningOnProfile(Profile profile, int machining)
        {
            SetMachiningOnProfileInternal(profile.__Ptr, machining);
            System.GC.KeepAlive(profile);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetMachiningOfProfile@8")]
        private static extern int GetMachiningOfProfileInternal(System.IntPtr profile, out int out_machining);

        public static int GetMachiningOfProfile(Profile profile, out int out_machining)
        {
            int returned_value = GetMachiningOfProfileInternal(profile.__Ptr, out out_machining);
            System.GC.KeepAlive(profile);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dIsProfileIgnoredByNesting@8")]
        private static extern int IsProfileIgnoredByNestingInternal(System.IntPtr profile, out bool out_nesting_ignored);

        public static int IsProfileIgnoredByNesting(Profile profile, out bool out_nesting_ignored)
        {
            int returned_value = IsProfileIgnoredByNestingInternal(profile.__Ptr, out out_nesting_ignored);
            System.GC.KeepAlive(profile);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetMachiningOnText@8")]
        private static extern void SetMachiningOnTextInternal(System.IntPtr text, int machining);

        public static void SetMachiningOnText(Text text, int machining)
        {
            SetMachiningOnTextInternal(text.__Ptr, machining);
            System.GC.KeepAlive(text);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetMachiningOfText@8")]
        private static extern int GetMachiningOfTextInternal(System.IntPtr text, out int out_machining);

        public static int GetMachiningOfText(Text text, out int out_machining)
        {
            int returned_value = GetMachiningOfTextInternal(text.__Ptr, out out_machining);
            System.GC.KeepAlive(text);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetBevelAttributeOnEdge@8")]
        private static extern int SetBevelAttributeOnEdgeInternal(System.IntPtr edge, int bevel_type);

        public static int SetBevelAttributeOnEdge(Edge edge, int bevel_type)
        {
            int returned_value = SetBevelAttributeOnEdgeInternal(edge.__Ptr, bevel_type);
            System.GC.KeepAlive(edge);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dRemoveBevelAttributeOnEdge@4")]
        private static extern void RemoveBevelAttributeOnEdgeInternal(System.IntPtr edge);

        public static void RemoveBevelAttributeOnEdge(Edge edge)
        {
            RemoveBevelAttributeOnEdgeInternal(edge.__Ptr);
            System.GC.KeepAlive(edge);
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dSetFieldOfBevelAttributeOnEdge@16")]
        private static extern int SetFieldOfBevelAttributeOnEdgeInternal(System.IntPtr edge, int field, double value);

        public static int SetFieldOfBevelAttributeOnEdge(Edge edge, int field, double value)
        {
            int returned_value = SetFieldOfBevelAttributeOnEdgeInternal(edge.__Ptr, field, value);
            System.GC.KeepAlive(edge);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetBevelTypeOnEdge@4")]
        private static extern int GetBevelTypeOnEdgeInternal(System.IntPtr edge);

        public static int GetBevelTypeOnEdge(Edge edge)
        {
            int returned_value = GetBevelTypeOnEdgeInternal(edge.__Ptr);
            System.GC.KeepAlive(edge);
            return returned_value;
        }

        [DllImport("ctopo2d.dll", CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi, EntryPoint = "_Topo2dGetFieldValueOfBevelAttributeOnEdge@12")]
        private static extern int GetFieldValueOfBevelAttributeOnEdgeInternal(System.IntPtr edge, int field, out double out_value);

        public static int GetFieldValueOfBevelAttributeOnEdge(Edge edge, int field, out double out_value)
        {
            int returned_value = GetFieldValueOfBevelAttributeOnEdgeInternal(edge.__Ptr, field, out out_value);
            System.GC.KeepAlive(edge);
            return returned_value;
        }

    }
}

#pragma warning restore 169
#pragma warning restore 649
