﻿using Drafter;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;
using Alma.NetWrappers2D;

using static Alma.NetWrappers2D.CutPartFactory;
using Controle_Pienap;
using System.Threading;
using System.Runtime.InteropServices;

namespace Actcut
{
    class Controle_Pienap
    {
        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool ShowWindow(System.IntPtr hWnd, int cmdShow);
        private static void Maximize()
        {
            ShowWindow(ThisConsole, RESTORE);
        }

        private const int HIDE = 0;
        private const int MAXIMIZE = 3;
        private const int MINIMIZE = 6;
        private const int RESTORE = 9;
        private static IntPtr ThisConsole = GetConsoleWindow();

        static readonly double permissibleError = 0.1;
        static readonly double epsilon = 0.0001;
        static void Main(string[] args)
        {
            string dprTempFile = "";
            Maximize();
            Console.Clear();
            if (args.Count() != 2 & args.Count() != 1)
            {
                Console.WriteLine("Usage: Controle_Pienap.exe filename");
                Console.WriteLine("  Controle_Pienap.exe \"c:\\Pema2\\X99_1234_56_789.xml\" \"c:\\Pema2\\X99_1234_56_789.dpr\"");
                Console.WriteLine();
                Console.WriteLine("Appuyez une touche pour continuer . . .");
                Console.ReadKey(true);
                return;
            }

            var executablePath = Assembly.GetExecutingAssembly().Location;
            Configuration config = ConfigurationManager.OpenExeConfiguration(executablePath);
            Drafter.Document drafterDoc = new Drafter.Document();

            dprFile dprf = null;
            if (args[0].ToLower() == "-redo")
            {
                if (config.AppSettings.Settings.Count > 0)
                    args = new string[] { config.AppSettings.Settings["xmlFileName"].Value, config.AppSettings.Settings["dprFileName"].Value };
                else
                {
                    Console.WriteLine("Pas d'historique");
                    Console.WriteLine();
                    Console.WriteLine("Appuyez une touche pour continuer . . .");
                    Console.ReadKey(true);
                    return;
                }
                dprf = new dprFile(args[1]);
                drafterDoc.FileOpen(dprf.prefix, dprf.filename, dprf.suffix, false, 1);
                drafterDoc.Application.Visible = true;
                Console.WriteLine("relaunch Controle_Pienap.exe \"" + args[0] + "\" \"" + args[1] + "\"");
            }
            string xmlFileName = args[0];
            string dprFileName = args[1];

            dprf = new dprFile(dprFileName);

            bool hasError = false;

            List<string> errorCfgFile = new List<string>();
            if (!File.Exists(xmlFileName))
            {
                errorCfgFile.Add(xmlFileName + " fichier introuvable");
                hasError = true;
            }

            if (!File.Exists(dprFileName))
            {
                errorCfgFile.Add(dprFileName + " fichier introuvable");
                hasError = true;
            }

            if (hasError)
            {
                errorCfgFile.Insert(0, "Controle_Pienap.exe \"" + xmlFileName + "\" \"" + dprFileName + "\"");
                ViewLog(errorCfgFile, dprf.filename);
                return;
            }

            config = ConfigurationManager.OpenExeConfiguration(executablePath);
            if (config.AppSettings.Settings["xmlFileName"] != null)
                config.AppSettings.Settings["xmlFileName"].Value = xmlFileName;
            else
                config.AppSettings.Settings.Add("xmlFileName", xmlFileName);

            if (config.AppSettings.Settings["dprFileName"] != null)
                config.AppSettings.Settings["dprFileName"].Value = dprFileName;
            else
                config.AppSettings.Settings.Add("dprFileName", dprFileName);
            config.Save();



            IContour actr;
            Drafter.Contours contoursToSave = new Drafter.Contours();

            drafterDoc.PartInfo();
            double xMaxw = (double)drafterDoc.Resource.GetGlobalParam(Drafter_res.IDS_OLE_BORNE_MAX_X).Value;
            double yMaxw = (double)drafterDoc.Resource.GetGlobalParam(Drafter_res.IDS_OLE_BORNE_MAX_Y).Value;

            if (Debugger.IsAttached)
            {
                drafterDoc.Application.Visible = true;
            }

            drafterDoc.Ungroup(drafterDoc.GetGroups(), true);
            Drafter.Contours ctrs = drafterDoc.GetContours();
            drafterDoc.View.Refresh();
            drafterDoc.Refresh();

            actr = ctrs.First();
            while (actr != null)
            {
                if (actr.Tooling.IsExterior && actr.IsClosed && actr.Tooling.Number == 2)
                    contoursToSave.Add(actr);
                actr = ctrs.Next();
            }

            dprTempFile = Path.GetTempFileName();
            dprFile dprFileTemp = new dprFile(dprTempFile);
            var ttt = contoursToSave.Count;
            drafterDoc.FileSaveSelection(contoursToSave, dprFileTemp.prefix, dprFileTemp.filename, dprFileTemp.suffix, 1);

            DprImporterPtr dprImporterPtr = NewDprImporter(dprTempFile);
            DprImporterImport(dprImporterPtr);
            AssociateDprMachiningToCpfMachiningByDefault(dprImporterPtr);
            PartFactoryPtr partFactoryPtr = NewDprPartFactory(dprImporterPtr);

            config.Save(ConfigurationSaveMode.Full, true);

            Console.WriteLine("Contrôle XML PEMA2");
            Console.WriteLine("\t Fichier:" + xmlFileName);
            Console.WriteLine();

            XmlSerializer serializer = new XmlSerializer(typeof(Pema2Xml));
            Pema2Xml pema2Xml;
            using (StringReader reader = new StringReader(File.ReadAllText(xmlFileName)))
            {
                pema2Xml = (Pema2Xml)serializer.Deserialize(reader);
            }

            string aproject = pema2Xml.Head.Project;
            string ablock = pema2Xml.Head.Block;
            string dpmFolder = Path.Combine(DefineAph.GetKeyPath("DPM"), aproject);
            dpmFolder = Path.Combine(dpmFolder, ablock).TrimEnd('\\') + "\\";

            PropertyInfo[] properties = typeof(Head).GetProperties();
            bool ParametersLength = properties.Count() > 0;
            bool DateTimeLength = pema2Xml.Head.DateTime.Length == 20;
            double panelMaxWidth = pema2Xml.Panel.MaxWidthValue;
            double panelMaxLength = pema2Xml.Panel.MaxLengthValue;

            int stiffenerCount = pema2Xml.Panel.Stiffeners.Stiffener.Count;
            int index = 0;
            double distance = 0;
            string errorText = string.Empty;

            Console.WriteLine("Contrôle des " + stiffenerCount + " raidisseurs");
            errorCfgFile.Add("Contrôle des " + stiffenerCount + " raidisseurs");
            foreach (Stiffener stiffener in pema2Xml.Panel.Stiffeners.Stiffener)
            {
                hasError = false;
                index++;
                int result = 0;
                if (stiffener.ReferencePoint.XPosition == null)
                {
                    errorCfgFile.Add("Raidisseur " + stiffener.Index + " (PosNo " + stiffener.PosNo + ") X=" + stiffener.Welds.XmaxWelds.XmaxWeld[0].XStartPoint + " Y=" + stiffener.Welds.XmaxWelds.XmaxWeld[0].YStartPoint);
                    errorCfgFile.Add("\tReference point NULL");
                    Debug.Print("Raidisseur " + stiffener.Index + " (PosNo " + stiffener.PosNo + ") X=" + stiffener.Welds.XmaxWelds.XmaxWeld[0].XStartPoint + " Y=" + stiffener.Welds.XmaxWelds.XmaxWeld[0].YStartPoint);
                }
                else
                {
                    errorCfgFile.Add("Raidisseur " + stiffener.Index + " (PosNo " + stiffener.PosNo + ") X=" + stiffener.ReferencePoint.XPosition + " Y=" + stiffener.ReferencePoint.YPosition);
                    Debug.Print("Raidisseur (" + stiffener.Index + ") X=" + stiffener.ReferencePoint.XPosition + " Y=" + stiffener.ReferencePoint.YPosition);
                }

                #region 3.1 - Stiffener.Reference.XPosition <= Panel.MaxLength
                distance = stiffener.ReferencePoint.XPositionValue;
                if (distance <= panelMaxLength)
                    result++;
                else
                {
                    hasError = true;
                    errorCfgFile.Add("\tReferencePoint.XPosition > panelMaxLength (" + distance + ">" + panelMaxLength + ")");
                    Debug.Print("\tReferencePoint.XPosition > panelMaxLength");
                }
                #endregion

                #region 3.2 - Stiffener.ContactLine.YEndPoint <= Panel.MaxWidth
                if (stiffener.Geometry.ContactLine != null)
                {
                    distance = stiffener.Geometry.ContactLine.YEndPointValue;
                    errorText = "(" + distance + ">" + panelMaxWidth + ")";
                    if (distance <= panelMaxWidth)
                        result++;
                    else
                    {
                        hasError = true;
                        errorCfgFile.Add("\tGeometry.ContactLine.YEndPoint > panelMaxWidth " + errorText);
                        Debug.Print("\tGeometry.ContactLine.YEndPoint > panelMaxWidth");
                    }
                }
                #endregion

                #region 4.1 - controle raidisseur YStart/YEnd inside contour
                bool startPointInProfile = false;
                bool endPointInProfile = false;
                int boundaryAccepted = 1;
                double xStartPos = -1;
                double yStartPos = -1;
                double xEndPos = -1;
                double yEndPos = -1;
                Topo2d.ProfilePtr raid = null;
                Topo2d.ProfilesPtr profilePtr = null;
                Topo2d.ProfileIteratorPtr iterator = null;
                Topo2d.ProfilePtr contourPtr = null;
                bool intersect = false;
                if (stiffener.Geometry.ContactLine != null)
                {
                    startPointInProfile = false;
                    endPointInProfile = false;
                    boundaryAccepted = 1;
                    xStartPos = stiffener.Geometry.ContactLine.XStartPointValue;
                    yStartPos = stiffener.Geometry.ContactLine.YStartPointValue;
                    xEndPos = stiffener.Geometry.ContactLine.XEndPointValue;
                    yEndPos = stiffener.Geometry.ContactLine.YEndPointValue;

                    raid = Topo2d.MakeRectangleProfileXY(xStartPos, yStartPos, xEndPos, yEndPos);

                    profilePtr = GetProfiles(partFactoryPtr, Machining.CutType);
                    iterator = Topo2d.NewProfileIterator(Topo2d.ConvertProfiles(profilePtr));

                    while (Topo2d.ProfileIteratorHasNext(iterator)) //  retrouve contour ou se trouve le raidisseur
                    {
                        Topo2d.Profile profile = Topo2d.ProfileIteratorNext(iterator);
                        contourPtr = Topo2d.DeepCopyProfile(profile);
                        if (Topo2d.IsInsideProfile(contourPtr, xStartPos, yStartPos, boundaryAccepted, permissibleError) == 1)
                            startPointInProfile = true;
                        if (Topo2d.IsInsideProfile(contourPtr, xEndPos, yEndPos, boundaryAccepted, permissibleError) == 1)
                            endPointInProfile = true;
                        if (startPointInProfile || endPointInProfile)
                            break;
                    }

                    if (contourPtr != null)
                        intersect = Topo2d.ProfilesIntersect(contourPtr, raid);

                    if (intersect)
                    {
                        hasError = true;
                        errorCfgFile.Add("\tRaidisseur croise un contour extérieur (X=" + xStartPos + " Y=" + yStartPos + ")");
                        Debug.Print("\tRaidisseur croise un contour extérieur (X=" + xStartPos + " Y=" + yStartPos + ")");
                    }
                }
                #endregion

                #region 4.2 - Stiffener.Geometry.SType <> Geometry.FlangeOrientation (Flat <> Null, Bulb <> Xmin/Xmax)
                string Stype = stiffener.Geometry.Stype;
                string FlangeOrientation = stiffener.Geometry.FlangeOrientation;
                if (Stype == "Flat")
                {
                    if (FlangeOrientation == null)
                        result++;
                    else
                    {
                        hasError = true;
                        errorCfgFile.Add("\t" + Stype + " = " + FlangeOrientation);
                        Debug.Print("\tFlangeOrientation " + Stype + " = " + FlangeOrientation);
                    }
                }
                else
                {
                    if (FlangeOrientation == "Xmin" || FlangeOrientation == "Xmax")
                        result++;
                    else
                    {
                        hasError = true;
                        errorCfgFile.Add("\t" + Stype + " = " + FlangeOrientation);
                        Debug.Print("\t FlangeOrientation" + Stype + " = " + FlangeOrientation);
                    }
                }
                #endregion

                #region ReferencePoint Xposition/Yposition = ContactLine Xstart/Ystart ou Xend/Yend
                if (stiffener.Geometry.ContactLine != null)
                {
                    double yposStart = stiffener.Geometry.ContactLine.YStartPointValue;
                    double yposEnd = stiffener.Geometry.ContactLine.YEndPointValue;
                    if (yposStart >= yposEnd)
                    {
                        hasError = true;
                        errorCfgFile.Add("\tDépart raidisseur > arrivée raidisseur (" + yposStart + ">=" + yposEnd + ")");
                        Debug.Print("\tDépart raidisseur > arrivée raidisseur (" + yposStart + ">=" + yposEnd + ")");
                    }
                }
                #endregion

                #region 4.3 - Stiffener.Welds.YStartPoint < Stiffener.Welds.YEndPoint
                foreach (XmaxWeld xmaxWeld in stiffener.Welds.XmaxWelds.XmaxWeld)
                {
                    errorText = "(" + xmaxWeld.YStartPointValue.ToString() + ">" + xmaxWeld.YEndPointValue.ToString() + ")";
                    if (xmaxWeld.YStartPointValue >= xmaxWeld.YEndPointValue)
                    {
                        hasError = true;
                        errorCfgFile.Add("\tWelds.XmaxWeld.YStartPoint > Welds.XmaxWeld.YEndPoint " + errorText);
                        Debug.Print("\tWelds.XmaxWeld.YStartPoint > Welds.XmaxWeld.YEndPoint");
                    }
                }
                foreach (XminWeld xminWeld in stiffener.Welds.XminWelds.XminWeld)
                {
                    errorText = "(" + xminWeld.YStartPointValue.ToString() + ">" + xminWeld.YEndPointValue.ToString() + ")";
                    if (xminWeld.YStartPointValue >= xminWeld.YEndPointValue)
                    {
                        hasError = true;
                        errorCfgFile.Add("Raidisseur (" + stiffener.Index + ") Welds.XminWeld.YStartPoint > Welds.XminWeld.YEndPoint " + errorText);
                        Debug.Print("Raidisseur (" + stiffener.Index + ") Welds.XminWeld.YStartPoint > Welds.XminWeld.YEndPoint");
                    }
                }
                #endregion

                #region 4.4 - Stiffener.Welds.YStartPoint >= Stiffener.Geometry.ContactLine.YStartPoint - 1.0 (ContactLine not accurate)
                if (stiffener.Geometry.ContactLine != null)
                {
                    foreach (XmaxWeld xmaxWeld in stiffener.Welds.XmaxWelds.XmaxWeld)
                    {
                        errorText = "(" + xmaxWeld.YStartPointValue.ToString() + "<" + stiffener.Geometry.ContactLine.YStartPointValue.ToString() + ")";
                        if (xmaxWeld.YStartPointValue >= (stiffener.Geometry.ContactLine.YStartPointValue - 1))
                            result++;
                        else
                        {
                            hasError = true;
                            errorCfgFile.Add("\tWelds.XmaxWeld.YStartPoint < Geometry.ContactLine.YStartPoint-1 " + errorText);
                            Debug.Print("\tWelds.XmaxWeld.YStartPoint < Geometry.ContactLine.YStartPoint-1");
                        }
                    }
                    foreach (XminWeld xminWeld in stiffener.Welds.XminWelds.XminWeld)
                    {
                        errorText = "(" + xminWeld.YStartPointValue.ToString() + "<" + stiffener.Geometry.ContactLine.YStartPointValue.ToString() + ")";
                        if (xminWeld.YStartPointValue >= (stiffener.Geometry.ContactLine.YStartPointValue - 1))
                            result++;
                        else
                        {
                            hasError = true;
                            errorCfgFile.Add("\tWelds.XminWeld.YStartPoint < Geometry.ContactLine.YStartPoint-1 " + errorText);
                            Debug.Print("\tWelds.XminWeld.YStartPoint < Geometry.ContactLine.YStartPoint-1");
                        }
                    }
                }
                #endregion

                #region 4.4 - Stiffener.Welds.YEndPoint <= Stiffener.Geomery.ContactLine.YEndPoint + 1.0 (ContactLine not accurate)
                if (stiffener.Geometry.ContactLine != null)
                {
                    foreach (XminWeld xminWeld in stiffener.Welds.XminWelds.XminWeld)
                    {
                        errorText = "(" + xminWeld.YEndPointValue.ToString() + ">" + (stiffener.Geometry.ContactLine.YEndPointValue + 1).ToString() + ")";
                        if (xminWeld.YEndPointValue <= (stiffener.Geometry.ContactLine.YEndPointValue + 1))
                            result++;
                        else
                        {
                            hasError = true;
                            errorCfgFile.Add("Raidisseur (" + stiffener.Index + ") Welds.XminWeld.YEndPoint > Geometry.ContactLine.YEndPoint+1 " + errorText);
                            Debug.Print("Raidisseur (" + stiffener.Index + ") Welds.XminWeld.YEndPoint > Geometry.ContactLine.YEndPoint+1");
                        }
                    }

                    foreach (XmaxWeld xmaxWeld in stiffener.Welds.XmaxWelds.XmaxWeld)
                    {
                        errorText = "(" + xmaxWeld.YEndPointValue.ToString() + ">" + (stiffener.Geometry.ContactLine.YEndPointValue + 1).ToString() + ")";
                        if (xmaxWeld.YEndPointValue <= (stiffener.Geometry.ContactLine.YEndPointValue + 1))
                            result++;
                        else
                        {
                            hasError = true;
                            errorCfgFile.Add("Raidisseur (" + stiffener.Index + ") Welds.XmaxWeld.YEndPoint > Geometry.ContactLine.YEndPoint+1 " + errorText);
                            Debug.Print("Raidisseur (" + stiffener.Index + ") Welds.XmaxWeld.YEndPoint > Geometry.ContactLine.YEndPoint+1");
                        }
                    }
                }
                #endregion

                #region 4.5 - Chevauchements des raidisseurs mini = 350 mm (avoid collision to SMWP magnet arms)
                //  check distance between stiffeners must be >= 350
                if (stiffener.Geometry.ContactLine != null)
                {
                    if (index < stiffenerCount)
                    {
                        Stiffener nextStiffener = pema2Xml.Panel.Stiffeners.Stiffener[index];

                        double xpos = stiffener.Geometry.ContactLine.XStartPointValue;
                        if (xpos < stiffener.Geometry.ContactLine.XEndPointValue)
                            xpos = stiffener.Geometry.ContactLine.XEndPointValue;

                        if (nextStiffener.Geometry.ContactLine != null)
                        {
                            double nextXpos = nextStiffener.Geometry.ContactLine.XStartPointValue;
                            if (nextXpos > nextStiffener.Geometry.ContactLine.XEndPointValue)
                                nextXpos = nextStiffener.Geometry.ContactLine.XEndPointValue;

                            distance = Math.Abs((double)(nextXpos - xpos));
                            if (distance < 350 && distance != 0)
                            {
                                hasError = true;
                                errorCfgFile.Add("Raidisseurs (" + stiffener.Index + "/" + nextStiffener.Index + ") overlapping distance < 350 (" + distance + ")");
                                Debug.Print("Raidisseurs (" + stiffener.Index + "/" + nextStiffener.Index + ") overlapping distance < 350");
                            }
                        }
                    }
                }
                #endregion

                #region 4.6 - Jeu de soudure >= 3mm
                //  check distance between stiffeners in the same X axis must be >= 3
                if (stiffener.Geometry.ContactLine != null)
                {
                    if (index < stiffenerCount)
                    {
                        Stiffener nextStiffener = pema2Xml.Panel.Stiffeners.Stiffener[index];

                        double xpos = stiffener.Geometry.ContactLine.XStartPointValue;
                        if (xpos < stiffener.Geometry.ContactLine.XEndPointValue)
                            xpos = stiffener.Geometry.ContactLine.XEndPointValue;
                        if (nextStiffener.Geometry.ContactLine != null)
                        {
                            double nextXpos = nextStiffener.Geometry.ContactLine.XStartPointValue;
                            if (nextXpos > nextStiffener.Geometry.ContactLine.XEndPointValue)
                                nextXpos = nextStiffener.Geometry.ContactLine.XEndPointValue;

                            distance = Math.Abs((double)(nextXpos - xpos));
                            if (distance == 0)
                            {
                                //  4.6 - Jeu de soudure >= 3mm
                                double ypos = stiffener.Geometry.ContactLine.YEndPointValue;
                                double nextYpos = nextStiffener.Geometry.ContactLine.YStartPointValue;
                                distance = Math.Abs((double)(nextYpos - ypos));
                                if (distance < 3)
                                {
                                    hasError = true;
                                    errorCfgFile.Add("Raidisseurs (" + stiffener.Index + "/" + nextStiffener.Index + ") gap < 3mm (" + distance + ")");
                                    Debug.Print("Raidisseurs (" + stiffener.Index + "/" + nextStiffener.Index + ") gap < 3mm (" + distance + ")");
                                }
                            }
                        }
                    }
                }
                #endregion

                #region 4.7 - Longueur de soudure
                double x1, x2, y1, y2, xMinWeldinglength, xMaxWeldinglength;
                foreach (XminWeld xminWeld in stiffener.Welds.XminWelds.XminWeld)
                {
                    x1 = xminWeld.XStartPointValue;
                    x2 = xminWeld.XEndPointValue;
                    y1 = xminWeld.YStartPointValue;
                    y2 = xminWeld.YEndPointValue;
                    xMinWeldinglength = Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
                    errorText = "(" + xMinWeldinglength + ">" + (stiffener.Geometry.TotalLengthValue).ToString() + "+1)";

                    if (xMinWeldinglength < (stiffener.Geometry.TotalLengthValue + 1))
                        result++;
                    else
                    {
                        hasError = true;
                        errorCfgFile.Add("\txMin Welding length > Geometry.ContactLine.TotalLength+1 " + errorText);
                        Debug.Print("\txMin Welding length > Geometry.ContactLine.TotalLength+1");
                    }

                    string fichierDPM = Directory.GetFiles(dpmFolder, ablock + stiffener.PosNo + "*.dpm").LastOrDefault();
                    if (fichierDPM == null)
                    {
                        hasError = true;
                        errorCfgFile.Add("\tDPM manquant " + ablock + stiffener.PosNo);
                        Debug.Print("\tDPM manquant " + ablock + stiffener.PosNo);
                    }
                    else
                    {
                        List<string> lines = File.ReadAllLines(fichierDPM).ToList();
                        int indexLongueur = lines.FindIndex(p => p.StartsWith("/LONGUEUR")) + 1;
                        string value = lines[indexLongueur].TrimStart('0');

                        if (stiffener.Geometry.TotalLength != value)
                        {
                            //  check DPM value
                            hasError = true;
                            errorCfgFile.Add("\tstiffener total length <> DPM value (" + stiffener.Geometry.TotalLength + " <> " + value + ")");
                            Debug.Print("\tstiffener total length <> DPM value (" + stiffener.Geometry.TotalLength + " <> " + value + ")");
                        }
                    }
                }

                foreach (XmaxWeld xmaxWeld in stiffener.Welds.XmaxWelds.XmaxWeld)
                {
                    x1 = xmaxWeld.XStartPointValue;
                    x2 = xmaxWeld.XEndPointValue;
                    y1 = xmaxWeld.YStartPointValue;
                    y2 = xmaxWeld.YEndPointValue;
                    xMaxWeldinglength = Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
                    errorText = "(" + xMaxWeldinglength + ">" + (stiffener.Geometry.TotalLengthValue).ToString() + "+1)";

                    if (xMaxWeldinglength < (stiffener.Geometry.TotalLengthValue + 1))
                        result++;
                    else
                    {
                        hasError = true;
                        errorCfgFile.Add("\txMax Welding length > Geometry.ContactLine.TotalLength+1 " + errorText);
                        Debug.Print("\txMax Welding length > Geometry.ContactLine.TotalLength+1");
                    }
                }
                #endregion

                #region 4.8 - controle soudures YStart/YEnd a l'interieur du contour
                startPointInProfile = false;
                endPointInProfile = false;
                boundaryAccepted = 1;
                foreach (XmaxWeld xmaxWeld in stiffener.Welds.XmaxWelds.XmaxWeld)
                {
                    xStartPos = xmaxWeld.XStartPointValue;
                    yStartPos = xmaxWeld.YStartPointValue;
                    xEndPos = xmaxWeld.XEndPointValue;
                    yEndPos = xmaxWeld.YEndPointValue;

                    profilePtr = GetProfiles(partFactoryPtr, Machining.CutType);
                    iterator = Topo2d.NewProfileIterator(Topo2d.ConvertProfiles(profilePtr));
                    intersect = false;

                    while (Topo2d.ProfileIteratorHasNext(iterator))
                    {
                        Topo2d.Profile profile = Topo2d.ProfileIteratorNext(iterator);
                        contourPtr = Topo2d.DeepCopyProfile(profile);
                        if (Topo2d.IsInsideProfile(contourPtr, xStartPos, yStartPos, boundaryAccepted, permissibleError) == 1)
                            startPointInProfile = true;
                        if (Topo2d.IsInsideProfile(contourPtr, xEndPos, yEndPos, boundaryAccepted, permissibleError) == 1)
                            endPointInProfile = true;
                        if (startPointInProfile || endPointInProfile)
                            break;
                    }

                    if (contourPtr != null && raid != null)
                        intersect = Topo2d.ProfilesIntersect(contourPtr, raid);

                    if (intersect)
                    {
                        hasError = true;
                        errorCfgFile.Add("\tSoudure croise un  contour extérieur (X=" + xStartPos + " Y=" + yStartPos + ")");
                        Debug.Print("\tSoudure croise un contour extérieur (X=" + xStartPos + " Y=" + yStartPos + ")");
                    }
                }
                #endregion

                #region 4.9 - controle raidisseur 50mm du bord d'un contour
                if (stiffener.Geometry.ContactLine != null)
                {
                    double stiffenerMinDistance = 50;

                    startPointInProfile = false;
                    endPointInProfile = false;
                    boundaryAccepted = 1;
                    xStartPos = stiffener.Geometry.ContactLine.XStartPointValue;
                    yStartPos = stiffener.Geometry.ContactLine.YStartPointValue;
                    xEndPos = stiffener.Geometry.ContactLine.XEndPointValue;
                    yEndPos = stiffener.Geometry.ContactLine.YEndPointValue;

                    Topo2d.ProfilePtr raidYend = Topo2d.MakeRectangleProfileXY(xStartPos - stiffenerMinDistance / 2, yStartPos, xEndPos + stiffenerMinDistance / 2, yStartPos + 2);
                    bool intersectEnd = false;
                    Topo2d.ProfilePtr raidYstart = Topo2d.MakeRectangleProfileXY(xStartPos - stiffenerMinDistance / 2, yEndPos, xEndPos + stiffenerMinDistance / 2, yEndPos - 2);
                    bool intersectStart = false;

                    profilePtr = GetProfiles(partFactoryPtr, Machining.CutType);
                    iterator = Topo2d.NewProfileIterator(Topo2d.ConvertProfiles(profilePtr));
                    while (Topo2d.ProfileIteratorHasNext(iterator))
                    {
                        Topo2d.Profile profile = Topo2d.ProfileIteratorNext(iterator);
                        contourPtr = Topo2d.DeepCopyProfile(profile);
                        if (Topo2d.IsInsideProfile(contourPtr, xStartPos, yStartPos, boundaryAccepted, permissibleError) == 1)
                            startPointInProfile = true;
                        if (Topo2d.IsInsideProfile(contourPtr, xEndPos, yEndPos, boundaryAccepted, permissibleError) == 1)
                            endPointInProfile = true;
                        if (startPointInProfile || endPointInProfile)
                            break;
                    }

                    if (contourPtr != null)
                    {
                        intersectStart = Topo2d.ProfilesIntersect(contourPtr, raidYstart);
                        intersectEnd = Topo2d.ProfilesIntersect(contourPtr, raidYend);
                    }
                    else
                    {
                        hasError = true;
                        errorCfgFile.Add("\tRaidisseur hors contour (X=" + xStartPos + " Y=" + yStartPos + ")");
                        Debug.Print("\tRaidisseur hors contour (X=" + xStartPos + " Y=" + yStartPos + ")");
                    }

                    if (intersectStart)
                    {
                        hasError = true;
                        errorCfgFile.Add("\tRaidisseur distance minimum non respectée (X=" + xStartPos + " Y=" + yStartPos + ")");
                        Debug.Print("\tRaidisseur distance minimum non respectée (X=" + xStartPos + " Y=" + yStartPos + ")");
                    }

                    if (intersectEnd)
                    {
                        hasError = true;
                        errorCfgFile.Add("\tRaidisseur distance minimum non respectée (X=" + xEndPos + " Y=" + yEndPos + ")");
                        Debug.Print("\tRaidisseur distance minimum non respectée (X=" + xEndPos + " Y=" + yEndPos + ")");
                    }

                    #endregion

                    if (hasError)
                        errorCfgFile.Add("");
                    else
                    {
                        errorCfgFile.Add("\tOk");
                        errorCfgFile.Add("");
                    }
                }
            }

            #region 4.1.3 - dimensions de la nappe
            if (panelMaxLength != xMaxw | panelMaxWidth != yMaxw)
            {
                errorCfgFile.Add("Dimensions incohérentes XML(DPR) " + panelMaxLength + "(" + xMaxw + ") " + panelMaxWidth + "(" + yMaxw + ")");
                Debug.Print("Dimensions incohérentes XML(DPR) " + panelMaxLength + "(" + xMaxw + ") " + panelMaxWidth + "(" + yMaxw + ")");
            }
            #endregion

            #region 4.1.4 - Controle balise Contours
            hasError = false;

            Elements elts;
            IElement aelt;
            Contours panelCtr = pema2Xml.Panel.Contours;
            Console.WriteLine("Contrôle des " + panelCtr.Contour.Count() + " contours");
            errorCfgFile.Add("Contrôle des " + panelCtr.Contour.Count() + " contours");
            int idx = 0;
            actr = contoursToSave.First();
            while (actr != null)
            {
                elts = drafterDoc.GetElements(actr);
                aelt = elts.First();
                while (aelt != null)
                {
                    double xs = aelt.Origin.X;
                    double ys = aelt.Origin.Y;
                    double xf = aelt.Extremity.X;
                    double yf = aelt.Extremity.Y;
                    double xc = aelt.Center.X;
                    double yc = aelt.Center.Y;

                    int indexElement = panelCtr.Contour.FindIndex(p =>
                    Math.Abs(p.XStartPointValue - xs) < epsilon &&
                    Math.Abs(p.YStartPointValue - ys) < epsilon &&
                    Math.Abs(p.XEndPointValue - xf) < epsilon &&
                    Math.Abs(p.YEndPointValue - yf) < epsilon &&
                    Math.Abs(p.XCenterValue - xc) < epsilon &&
                    Math.Abs(p.YCenterValue - yc) < epsilon);

                    if (indexElement == -1)
                    {
                        hasError = true;
                        errorCfgFile.Add("Contour incohérent (X1=" + xs + " Y1=" + ys + " X2=" + xf + " Y2=" + yf + " Xc=" + xc + " Yc=" + yc);
                        Debug.Print("Contour incohérent (X1=" + xs + " Y1=" + ys + " X2=" + xf + " Y2=" + yf + " Xc=" + xc + " Yc=" + yc);
                    }
                    aelt = elts.Next();
                    idx++;
                }
                actr = contoursToSave.Next();
            }
            if (hasError)
                errorCfgFile.Add("");
            else
            {
                errorCfgFile.Add("\tOk");
                errorCfgFile.Add("");
            }
            #endregion

            #region 4.10 - Coordonnees raidisseur mini 50mm du bord
            #endregion

            if (File.Exists(dprTempFile))
                File.Delete(dprTempFile);

            Console.WriteLine("Traitement terminé . . .");
            Thread.Sleep(1000);

            if (errorCfgFile.Count() > 0)
                ViewLog(errorCfgFile, dprf.filename);
            drafterDoc.FileOpen(dprf.prefix, dprf.filename, dprf.suffix, false, 1);
        }

        private static void ViewLog(List<string> errorCfgFile, string filename)
        {
            string logFile = Path.Combine(DefineAph.GetKeyPath("LOG"), filename + ".log");
            using (StreamWriter sw = new StreamWriter(logFile))
            {
                foreach (string line in errorCfgFile)
                {
                    sw.WriteLine(line);
                }
            }
            Process p = new Process();
            ProcessStartInfo processStartInfo = new ProcessStartInfo("notepad.exe", logFile);
            processStartInfo.WindowStyle = ProcessWindowStyle.Normal;
            p.StartInfo = processStartInfo;
            p.Start();

        }
        private static T MysConvert<T>(string value) where T : struct, IConvertible
        {
            bool isDoubleValue = double.TryParse(value, out double doubleResult);
            bool isIntegerValue = int.TryParse(value, out int integerValue);
            if (isIntegerValue && typeof(T) == typeof(int))
                return (T)Convert.ChangeType(integerValue, typeof(int));
            else if (isDoubleValue)
                return (T)Convert.ChangeType(doubleResult, typeof(double));
            else
            {
                string result = (string)value;
                return (T)Convert.ChangeType(result, typeof(string));
            }
        }
    }
    public class dprFile
    {
        public string prefix = "";
        public string filename = "";
        public string suffix = "";
        public dprFile(string dprFileName)
        {
            prefix = Path.GetDirectoryName(dprFileName) + "\\";
            filename = Path.GetFileNameWithoutExtension(dprFileName);
            suffix = Path.GetExtension(dprFileName);
        }
    }
}
