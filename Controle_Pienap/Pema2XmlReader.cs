﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Actcut
{
    public static class DecimalSeparator
    {
        static private readonly string val = Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
        public static string Value(string _value)
        {
            return _value.Replace(".", val).Replace(",", val);
        }
    }

    [XmlRoot(ElementName = "Head")]
    public class Head
    {

        [XmlElement(ElementName = "Project")]
        public string Project { get; set; }

        [XmlElement(ElementName = "Block")]
        public string Block { get; set; }

        [XmlElement(ElementName = "Path")]
        public string Path { get; set; }

        [XmlElement(ElementName = "Author")]
        public string Author { get; set; }

        [XmlElement(ElementName = "Tool")]
        public string Tool { get; set; }

        [XmlElement(ElementName = "ToolVersion")]
        public string ToolVersion { get; set; }

        [XmlElement(ElementName = "DateTime")]
        public string DateTime { get; set; }
    }

    [XmlRoot(ElementName = "Contour")]
    public class Contour
    {

        [XmlElement(ElementName = "Ctype")]
        public string Ctype { get; set; }

        [XmlElement(ElementName = "Direction")]
        public string Direction { get; set; }

        [XmlElement(ElementName = "XCenter")]
        private string _XCenter;
        public double XCenterValue;
        public string XCenter
        {
            get { return _XCenter; }
            set
            {
                if (value == "NULL")
                    _XCenter = "0";
                else
                    _XCenter = DecimalSeparator.Value(value);
                XCenterValue = double.Parse(_XCenter);
            }
        }

        [XmlElement(ElementName = "YCenter")]
        private string _YCenter;
        public double YCenterValue;
        public string YCenter
        {
            get { return _YCenter; }
            set
            {
                if (value == "NULL")
                    _YCenter = "0";
                else
                    _YCenter = DecimalSeparator.Value(value);
                YCenterValue = double.Parse(_YCenter);
            }
        }

        [XmlElement(ElementName = "XStartPoint")]
        private string _XStartPoint;
        public double XStartPointValue;
        public string XStartPoint
        {
            get { return _XStartPoint; }
            set
            {
                _XStartPoint = DecimalSeparator.Value(value);
                XStartPointValue = double.Parse(_XStartPoint);
            }
        }

        [XmlElement(ElementName = "YStartPoint")]
        private string _YStartPoint;
        public double YStartPointValue;
        public string YStartPoint
        {
            get { return _YStartPoint; }
            set
            {
                _YStartPoint = DecimalSeparator.Value(value);
                YStartPointValue = double.Parse(_YStartPoint);
            }
        }

        [XmlElement(ElementName = "XEndPoint")]
        private string _XEndPoint;
        public double XEndPointValue;
        public string XEndPoint
        {
            get { return _XEndPoint; }
            set
            {
                _XEndPoint = DecimalSeparator.Value(value);
                XEndPointValue = double.Parse(_XEndPoint);
            }
        }

        [XmlElement(ElementName = "YEndPoint")]
        private string _YEndPoint;
        public double YEndPointValue;
        public string YEndPoint
        {
            get { return _YEndPoint; }
            set
            {
                _YEndPoint = DecimalSeparator.Value(value);
                YEndPointValue = double.Parse(_YEndPoint);
            }
        }

        [XmlAttribute(AttributeName = "index")]
        public int Index { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Contours")]
    public class Contours
    {

        [XmlElement(ElementName = "Contour")]
        public List<Contour> Contour { get; set; }
    }

    [XmlRoot(ElementName = "ReferencePoint")]
    public class ReferencePoint
    {

        [XmlElement(ElementName = "Side")]
        public string Side { get; set; }

        [XmlElement(ElementName = "XPosition")]
        private string _XPosition;
        public double XPositionValue;
        public string XPosition
        {
            get { return _XPosition; }
            set
            {
                _XPosition = DecimalSeparator.Value(value);
                XPositionValue = double.Parse(_XPosition);
            }
        }

        [XmlElement(ElementName = "YPosition")]
        private string _YPosition;
        public double YPositionValue;
        public string YPosition
        {
            get { return _YPosition; }
            set
            {
                _YPosition = DecimalSeparator.Value(value);
                YPositionValue = double.Parse(_YPosition);
            }
        }
    }

    [XmlRoot(ElementName = "ContactLine")]
    public class ContactLine
    {

        [XmlElement(ElementName = "XStartPoint")]
        private string _XStartPoint;
        public double XStartPointValue;
        public string XStartPoint
        {
            get { return _XStartPoint; }
            set
            {
                _XStartPoint = DecimalSeparator.Value(value);
                XStartPointValue = double.Parse(_XStartPoint);
            }
        }

        [XmlElement(ElementName = "YStartPoint")]
        private string _YStartPoint;
        public double YStartPointValue;
        public string YStartPoint
        {
            get { return _YStartPoint; }
            set
            {
                _YStartPoint = DecimalSeparator.Value(value);
                YStartPointValue = double.Parse(_YStartPoint);
            }
        }

        [XmlElement(ElementName = "XEndPoint")]
        private string _XEndPoint;
        public double XEndPointValue;
        public string XEndPoint
        {
            get { return _XEndPoint; }
            set
            {
                _XEndPoint = DecimalSeparator.Value(value);
                XEndPointValue = double.Parse(_XEndPoint);
            }
        }

        [XmlElement(ElementName = "YEndPoint")]
        private string _YEndPoint;
        public double YEndPointValue;
        public string YEndPoint
        {
            get { return _YEndPoint; }
            set
            {
                _YEndPoint = DecimalSeparator.Value(value);
                YEndPointValue = double.Parse(_YEndPoint);
            }
        }
    }

    [XmlRoot(ElementName = "Geometry")]
    public class Geometry
    {

        [XmlElement(ElementName = "Stype")]
        public string Stype { get; set; }

        [XmlElement(ElementName = "FlangeOrientation")]
        public string FlangeOrientation { get; set; }

        [XmlElement(ElementName = "TotalLength")]
        private string _TotalLength;
        public double TotalLengthValue;
        public string TotalLength
        {
            get { return _TotalLength; }
            set
            {
                _TotalLength = DecimalSeparator.Value(value);
                TotalLengthValue = double.Parse(_TotalLength);
            }
        }

        [XmlElement(ElementName = "Height")]
        private string _Height;
        public double HeightValue;
        public string Height
        {
            get { return _Height; }
            set
            {
                _Height = DecimalSeparator.Value(value);
                HeightValue = double.Parse(_Height);
            }
        }

        [XmlElement(ElementName = "Quality")]
        public string Quality { get; set; }

        [XmlElement(ElementName = "ThicknessWeb")]
        private string _ThicknessWeb;
        public double ThicknessWebValue;
        public string ThicknessWeb
        {
            get { return _ThicknessWeb; }
            set
            {
                _ThicknessWeb = DecimalSeparator.Value(value);
                ThicknessWebValue = double.Parse(_ThicknessWeb);
            }
        }

        [XmlElement(ElementName = "YminOffset")]
        public string YminOffset { get; set; }

        [XmlElement(ElementName = "YmaxOffset")]
        public string YmaxOffset { get; set; }

        [XmlElement(ElementName = "ContactLine")]
        public ContactLine ContactLine { get; set; }
    }

    [XmlRoot(ElementName = "XmaxWeld")]
    public class XmaxWeld
    {
        [XmlElement(ElementName = "XStartPoint")]
        private string _XStartPoint;
        public double XStartPointValue;
        public string XStartPoint
        {
            get { return _XStartPoint; }
            set
            {
                _XStartPoint = DecimalSeparator.Value(value);
                XStartPointValue = double.Parse(_XStartPoint);
            }
        }

        [XmlElement(ElementName = "YStartPoint")]
        private string _YStartPoint;
        public double YStartPointValue;
        public string YStartPoint
        {
            get { return _YStartPoint; }
            set
            {
                _YStartPoint = DecimalSeparator.Value(value);
                YStartPointValue = double.Parse(_YStartPoint);
            }
        }

        [XmlElement(ElementName = "XEndPoint")]
        private string _XEndPoint;
        public double XEndPointValue;
        public string XEndPoint
        {
            get { return _XEndPoint; }
            set
            {
                _XEndPoint = DecimalSeparator.Value(value);
                XEndPointValue = double.Parse(_XEndPoint);
            }
        }

        [XmlElement(ElementName = "YEndPoint")]
        private string _YEndPoint;
        public double YEndPointValue;
        public string YEndPoint
        {
            get { return _YEndPoint; }
            set
            {
                _YEndPoint = DecimalSeparator.Value(value);
                YEndPointValue = double.Parse(_YEndPoint);
            }
        }

        [XmlAttribute(AttributeName = "index")]
        public int Index { get; set; }

        [XmlText]
        public string Text { get; set; }
    }
    [XmlRoot(ElementName = "XmaxWelds")]
    public class XmaxWelds
    {
        [XmlElement(ElementName = "XmaxWeld")]
        public List<XmaxWeld> XmaxWeld { get; set; }
    }

    [XmlRoot(ElementName = "XminWeld")]
    public class XminWeld
    {
        [XmlElement(ElementName = "XStartPoint")]
        private string _XStartPoint;
        public double XStartPointValue;
        public string XStartPoint
        {
            get { return _XStartPoint; }
            set
            {
                _XStartPoint = DecimalSeparator.Value(value);
                XStartPointValue = double.Parse(_XStartPoint);
            }
        }

        [XmlElement(ElementName = "YStartPoint")]
        private string _YStartPoint;
        public double YStartPointValue;
        public string YStartPoint
        {
            get { return _YStartPoint; }
            set
            {
                _YStartPoint = DecimalSeparator.Value(value);
                YStartPointValue = double.Parse(_YStartPoint);
            }
        }

        [XmlElement(ElementName = "XEndPoint")]
        private string _XEndPoint;
        public double XEndPointValue;
        public string XEndPoint
        {
            get { return _XEndPoint; }
            set
            {
                _XEndPoint = DecimalSeparator.Value(value);
                XEndPointValue = double.Parse(_XEndPoint);
            }
        }

        [XmlElement(ElementName = "YEndPoint")]
        private string _YEndPoint;
        public double YEndPointValue;
        public string YEndPoint
        {
            get { return _YEndPoint; }
            set
            {
                _YEndPoint = DecimalSeparator.Value(value);
                YEndPointValue = double.Parse(_YEndPoint);
            }
        }

        [XmlAttribute(AttributeName = "index")]
        public int Index { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "XminWelds")]
    public class XminWelds
    {
        [XmlElement(ElementName = "XminWeld")]
        public List<XminWeld> XminWeld { get; set; }
    }

    [XmlRoot(ElementName = "Welds")]
    public class Welds
    {

        [XmlElement(ElementName = "ToDo")]
        public string ToDo { get; set; }

        [XmlElement(ElementName = "XmaxWelds")]
        public XmaxWelds XmaxWelds { get; set; }

        [XmlElement(ElementName = "XminWelds")]
        public XminWelds XminWelds { get; set; }
    }

    [XmlRoot(ElementName = "Stiffener")]
    public class Stiffener
    {

        [XmlElement(ElementName = "PosNo")]
        public string PosNo { get; set; }

        [XmlElement(ElementName = "LinkedPlateThickness")]
        public int LinkedPlateThickness { get; set; }

        [XmlElement(ElementName = "LinkedPlateQuality")]
        public string LinkedPlateQuality { get; set; }

        [XmlElement(ElementName = "ReferencePoint")]
        public ReferencePoint ReferencePoint { get; set; }

        [XmlElement(ElementName = "Geometry")]
        public Geometry Geometry { get; set; }

        [XmlElement(ElementName = "Welds")]
        public Welds Welds { get; set; }

        [XmlAttribute(AttributeName = "index")]
        public int Index { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Stiffeners")]
    public class Stiffeners
    {
        [XmlElement(ElementName = "Stiffener")]
        public List<Stiffener> Stiffener { get; set; }
    }

    [XmlRoot(ElementName = "Panel")]
    public class Panel
    {

        [XmlElement(ElementName = "MaxLength")]
        private string _MaxLength;
        public double MaxLengthValue;
        public string MaxLength
        {
            get { return _MaxLength; }
            set
            {
                _MaxLength = DecimalSeparator.Value(value);
                MaxLengthValue = double.Parse(_MaxLength);
            }
        }

        [XmlElement(ElementName = "MaxWidth")]
        private string _MaxWidth;
        public double MaxWidthValue;
        public string MaxWidth
        {
            get { return _MaxWidth; }
            set
            {
                _MaxWidth = DecimalSeparator.Value(value);
                MaxWidthValue = double.Parse(_MaxWidth);
            }
        }

        [XmlElement(ElementName = "Contours")]
        public Contours Contours { get; set; }

        [XmlElement(ElementName = "Stiffeners")]
        public Stiffeners Stiffeners { get; set; }
    }

    [XmlRoot(ElementName = "Root")]
    public class Pema2Xml
    {

        [XmlElement(ElementName = "Head")]
        public Head Head { get; set; }

        [XmlElement(ElementName = "Panel")]
        public Panel Panel { get; set; }

        [XmlAttribute(AttributeName = "sql")]
        public string Sql { get; set; }

        [XmlText]
        public string Text { get; set; }
    }
}
