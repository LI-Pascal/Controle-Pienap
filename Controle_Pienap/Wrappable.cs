﻿namespace Alma.NetWrappers
{
    public abstract class Wrappable
    {
        public System.IntPtr __Ptr;

        public static T Create<T>(System.IntPtr ptr, T t) where T : Wrappable
        {
            if (ptr != System.IntPtr.Zero)
            {
                t.__Ptr = ptr;
                return t;
            }
            return null;
        }
    }
}